/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <Qt3DRender>
#include <QtDebug>
#include <QAttribute>
#include <QGeometry>
#include <QGeometryFactory>
#include <QBullet/btIndexedMeshHelper.h>
#include "TriangleMeshGeometry.h"

using namespace Qt3DCore;
using namespace Qt3DRender;

namespace QBullet {

class VertexDataFunctor : public QBufferDataGenerator
{
public:
    QT3D_FUNCTOR(VertexDataFunctor)
    VertexDataFunctor(TriangleMesh *mesh)
        : m_helper(mesh->mesh())
        , m_version(0)
    {
        if(m_helper)
            m_version = m_helper->vertexVersion();
    }

    QByteArray operator ()() Q_DECL_OVERRIDE
    {
        return m_helper?m_helper->vertexBuffer():QByteArray();
    }

    bool operator ==(const QBufferDataGenerator &other) const Q_DECL_OVERRIDE
    {
        const VertexDataFunctor *otherFunctor = functor_cast<VertexDataFunctor>(&other);
        if (otherFunctor != nullptr)
            return otherFunctor->m_helper==m_helper&&
                    m_version==otherFunctor->m_version;
        return false;
    }

private:
    QSharedPointer<btIndexedMeshHelper> m_helper;
    long m_version;
};


TriangleMeshGeometry::TriangleMeshGeometry(TriangleMesh *mesh, Qt3DCore::QNode *parent)
    : QGeometry(parent)
    , m_mesh(mesh)
{
    if(m_mesh) {
        m_helper = m_mesh->mesh();
        connect(m_mesh, &TriangleMesh::changed, this, &TriangleMeshGeometry::updateVertexBuffer);
    }

    if(m_helper) {
        // No need to worry about memory of buffers, once they are added to a
        // geometry, they will be released by Qt3D engine once used.

        m_vertexBuffer = new Qt3DRender::QBuffer;
        m_vertexBuffer->setUsage(Qt3DRender::QBuffer::DynamicDraw);
        //Can use a functor to control vertex buffer update in a finer level.
        m_vertexBuffer->setDataGenerator(QBufferDataGeneratorPtr(new VertexDataFunctor(m_mesh)));

        m_indexBuffer = new Qt3DRender::QBuffer;
        m_indexBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
        m_indexBuffer->setData(m_helper->indexBuffer());

        m_texCoordBuffer = new Qt3DRender::QBuffer;
        m_texCoordBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
        m_texCoordBuffer->setData(m_helper->texCoordBuffer());

        // Fill in the mesh
        setObjectName("TriangleMeshGeometry");

        // positions and normals are in the same vertex buffer.
        // positions (float x, float y, float z) , normal(float nx, float ny, float nz)
        quint32 stride = sizeof(QBullet::VertexData);

        addAttribute(new QAttribute(
                               m_vertexBuffer,
                               QAttribute::defaultPositionAttributeName(),
                               QAttribute::Float,
                               3,
                               m_helper->m_numVertices,
                               0,
                               stride));

        quint32 offset = sizeof(float) * 3;

        //Normal
        addAttribute(new QAttribute(
                               m_vertexBuffer,
                               QAttribute::defaultNormalAttributeName(),
                               QAttribute::Float,
                               3,
                               m_helper->m_numVertices,
                               offset,
                               stride));

        //Texture Coordinates
        addAttribute(new QAttribute(
                               m_texCoordBuffer,
                               QAttribute::defaultTextureCoordinateAttributeName(),
                               QAttribute::Float,
                               2,
                               m_helper->m_numVertices,
                               0,
                               sizeof(float)*2));

        /*
        //Tangent
        mesh->addAttribute(Qt3D::QMeshData::defaultTangentAttributeName(), Qt3D::AttributePtr(new Qt3D::Attribute(buf, GL_FLOAT_VEC4, vertexCount, offset, stride)));
        offset += sizeof(float) * 4;
        */

        /*
        Must use QAttribute::UnsignedInt. Can not use QAttribute::Int
        */
        QAttribute *indexAttribute = new QAttribute(m_indexBuffer, QAttribute::UnsignedInt, 1, m_helper->indexCount());
        indexAttribute->setAttributeType(QAttribute::IndexAttribute);

        addAttribute(indexAttribute);
    }
}

void TriangleMeshGeometry::updateVertexBuffer()
{
    if(m_vertexBuffer) {
        m_vertexBuffer->setDataGenerator(QBufferDataGeneratorPtr(new VertexDataFunctor(m_mesh)));
    }
}



}
