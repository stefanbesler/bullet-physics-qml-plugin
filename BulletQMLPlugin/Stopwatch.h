/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <QElapsedTimer>
#include <QHash>
#include <QMutex>
#include <QString>

/*!
\class Stopwatch Stopwatch.h <Common/Stopwatch.h>

The Stopwatch class is used to profile the code segment.

\code

{
    Stopwatch stopwatch("MyCodeSegment");

    ...//my code segment goese here.

}   // At the end of the scope , the execute time is output as debug message.

int maxTimeOfMyCodeSegment = Stopwatch::maxTime("MyCodeSegment"); //Get the max execution time of my code segment.

\endcode

*/
class Stopwatch
{
public:
    Stopwatch(const QString &name, bool outputDebug = true);

    ~Stopwatch();

    struct Data
    {
        Data();

        qint64 min, max;
        qint64 hits;
        qint64 avg;
        qint64 total;
        qint64 last;
        void push(qint64 t);
        QString toString() const;
    };

    qint64 elapsed() const;

    static Data data(const QString &name);

    static qint64 maxTime(const QString &name);

    static void clearMaxTime(const QString &name);

    static QStringList keys();

private:
    QString _name;
    bool _outputDebug;
    QElapsedTimer _timer;
    static QHash<QString, Data> _data;
};

#endif // STOPWATCH_H
