/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "btIndexedMeshHelper.h"
#include "TriangleMesh.h"

namespace QBullet {

TriangleMesh::TriangleMesh(QObject *parent)
    : BulletObject(parent)
{

}

QSharedPointer<btIndexedMeshHelper> TriangleMesh::mesh() const
{
    return _mesh;
}

void TriangleMesh::setMesh(QSharedPointer<btIndexedMeshHelper> mesh)
{
    if(_mesh==mesh)
        return;
    bool wasValid = isValid();
    _mesh = mesh;
    if(wasValid!=isValid())
        emit meshReset(isValid());
}

void TriangleMesh::setChanged()
{
    if(_mesh)
        _mesh->increaseVertexVersion();
    emit changed();
}

bool TriangleMesh::isValid() const
{
    if(_mesh.isNull()||
            _mesh->m_numTriangles==0||
            _mesh->m_numVertices==0||
            _mesh->m_triangleIndexBase==0||
            _mesh->m_vertexBase==0) {
        return false;
    }
    return true;
}

}

#include "moc_TriangleMesh.cpp"
