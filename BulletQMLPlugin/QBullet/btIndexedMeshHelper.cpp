/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QScopedPointer>
#include <QVector>
#include "btIndexedMeshHelper.h"

namespace QBullet {

IndexData::IndexData() {
    a = b = c = 0.0;
}

VertexData::VertexData() {
    x = y = z = 0.0;
    //tx = ty = 0.0;
    nx = ny = nz = 0.0;
}

btIndexedMeshHelper::btIndexedMeshHelper(int numOfVertices, int numOfTriangles)
    : m_triangles(0)
    , m_vertices(0)
    , m_version(0)
{
    m_numTriangles = numOfTriangles;
    m_numVertices = numOfVertices;
    Q_ASSERT(numOfTriangles>0&&numOfVertices>0);

    m_vertexStride = sizeof(VertexData);
    m_vertices.resize(numOfVertices);
    m_vertexBase = reinterpret_cast<const unsigned char *>(m_vertices.data());

    m_texCoords.resize(numOfVertices);

    m_triangles.resize(numOfTriangles);
    m_triangleIndexBase = reinterpret_cast<const unsigned char *>(m_triangles.data());
    m_triangleIndexStride = sizeof(IndexData);
}

btIndexedMeshHelper::~btIndexedMeshHelper() {
}

void btIndexedMeshHelper::triangle(int index, int &a, int &b, int &c) const
{
    a = m_triangles[index].a;
    b = m_triangles[index].b;
    c = m_triangles[index].c;
}

void btIndexedMeshHelper::setTriangle(int index, int a, int b, int c) {
    m_triangles[index].a = a;
    m_triangles[index].b = b;
    m_triangles[index].c = c;
}

void btIndexedMeshHelper::vertex(int index, btScalar &x, btScalar &y, btScalar &z) const
{
    x = m_vertices[index].x;
    y = m_vertices[index].y;
    z = m_vertices[index].z;
}

QVector3D btIndexedMeshHelper::vertex(int index) const
{
    return QVector3D(m_vertices[index].x, m_vertices[index].y, m_vertices[index].z);
}

QVector3D btIndexedMeshHelper::normal(int index) const
{
    return QVector3D(m_vertices[index].nx, m_vertices[index].ny, m_vertices[index].nz);
}

QPointF btIndexedMeshHelper::texCoord(int index) const
{
    return QPointF(m_texCoords[index].tx, m_texCoords[index].ty);
}

void btIndexedMeshHelper::setTexCoord(int index, QPointF coord)
{
    m_texCoords[index].tx = coord.x();
    m_texCoords[index].ty = coord.y();
}

void btIndexedMeshHelper::setVertex(int index, const QVector3D &v)
{
    setVertex(index, v.x(), v.y(), v.z());
}

void btIndexedMeshHelper::setVertex(int index, btScalar x, btScalar y, btScalar z) {
    m_vertices[index].x = x;
    m_vertices[index].y = y;
    m_vertices[index].z = z;
}

void btIndexedMeshHelper::setNormal(int index, const QVector3D &n)
{
    setNormal(index, n.x(), n.y(), n.z());
}

void btIndexedMeshHelper::setNormal(int index, btScalar nx, btScalar ny, btScalar nz)
{
    m_vertices[index].nx = nx;
    m_vertices[index].ny = ny;
    m_vertices[index].nz = nz;
}

void btIndexedMeshHelper::updateVertexNormal()
{
    QScopedPointer<QVector<QVector3D> > tmpNormals(new QVector<QVector3D>(m_numVertices));
    QScopedPointer<QVector<int> > tmpCnt(new QVector<int>(m_numVertices));
    tmpNormals->fill(QVector3D());
    tmpCnt->fill(0);

    for(int i=0; i<m_numTriangles; i++) {
        int a, b, c;
        triangle(i, a, b, c);
        QVector3D va = vertex(a);
        QVector3D vb = vertex(b);
        QVector3D vc = vertex(c);
        QVector3D n = QVector3D::normal(va, vb, vc);
        (*tmpNormals)[a]+=n;
        (*tmpNormals)[b]+=n;
        (*tmpNormals)[c]+=n;
        (*tmpCnt)[a]++;
        (*tmpCnt)[b]++;
        (*tmpCnt)[c]++;
    }
    for(int i=0; i<m_numVertices; i++) {
        QVector3D n;
        if((*tmpCnt)[i]>0) {
            n = (*tmpNormals)[i]/(*tmpCnt)[i];
        }
        m_vertices[i].nx = n.x();
        m_vertices[i].ny = n.y();
        m_vertices[i].nz = n.z();
    }
}

int btIndexedMeshHelper::indexCount() const
{
    return m_numTriangles * 3;
}

QByteArray btIndexedMeshHelper::texCoordBuffer() const
{
    return QByteArray(reinterpret_cast<const char *>(m_texCoords.data()), sizeof(TexCoordData)*m_numVertices);
}

QByteArray btIndexedMeshHelper::vertexBuffer() const
{
    return QByteArray(reinterpret_cast<const char *>(m_vertexBase), sizeof(VertexData)*m_numVertices);
}

QByteArray btIndexedMeshHelper::indexBuffer() const
{
    return QByteArray(reinterpret_cast<const char *>(m_triangleIndexBase), sizeof(IndexData)*m_numTriangles);
}

long btIndexedMeshHelper::vertexVersion() const
{
    return m_version;
}

void btIndexedMeshHelper::increaseVertexVersion()
{
    m_version++;
}

TexCoordData::TexCoordData()
{
    tx = ty = 0.0;
}

}
