/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef BTINDEXEDMESHHELPER_H
#define BTINDEXEDMESHHELPER_H

#include <QByteArray>
#include <QPointF>
#include <QVector>
#include <QVector3D>
#include <btBulletDynamicsCommon.h>
namespace QBullet {

#pragma pack(push, 1)
struct IndexData{
    IndexData();
    int a, b, c;
};

struct TexCoordData {
    TexCoordData();
    btScalar tx, ty;//tex coordinate.
};

struct VertexData {
    VertexData();
    btScalar x, y, z;//Vertex
    //btScalar tx, ty;//tex coordinate.
    btScalar nx, ny, nz;//normal;
};

#pragma pack(pop)

class btIndexedMeshHelper: public btIndexedMesh
{
public:
    btIndexedMeshHelper(int numOfVertices, int numOfTriangles);

    ~btIndexedMeshHelper();

    void triangle(int index, int &a, int &b, int &c) const;

    void setTriangle(int index, int a, int b, int c);

    void vertex(int index, btScalar &x, btScalar &y, btScalar &z) const;

    QVector3D vertex(int index) const;

    QVector3D normal(int index) const;

    QPointF texCoord(int index) const;

    void setTexCoord(int index, QPointF coord);

    void setVertex(int index, const QVector3D &v);

    void setVertex(int index, btScalar x, btScalar y, btScalar z);

    void setNormal(int index, const QVector3D &n);

    void setNormal(int index, btScalar nx, btScalar ny, btScalar nz);

    void updateVertexNormal();

    int indexCount() const;

    QByteArray texCoordBuffer() const;

    QByteArray vertexBuffer() const;

    QByteArray indexBuffer() const;

    long vertexVersion() const;

    void increaseVertexVersion();

private:

    QVector<TexCoordData> m_texCoords;
    QVector<IndexData> m_triangles;
    QVector<VertexData> m_vertices;
    //everytime the mesh is updated should increase the version.
    long m_version;
};

}

#endif // BTINDEXEDMESHHELPER_H
