/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef POINT2POINTCONSTRAINT_H
#define POINT2POINTCONSTRAINT_H

#include <QVector3D>
#include "Constraint.h"

class btPoint2PointConstraint;

namespace QBullet
{

/*!
\class Point2PointConstraint Point2PointConstraint.h <QBullet/Point2PointConstraint.h>

The point to point constraint, also known as ball socket joint, limits the
translation so that the local pivot points of two rigidbodies match in
worldspace. A chain of rigidbodies can be connected using this constraint.

\see http://bulletphysics.org/mediawiki-1.5.8/index.php/Constraints
*/
class Point2PointConstraint : public Constraint
{
    Q_OBJECT
    Q_PROPERTY(QVector3D pivotA READ pivotA WRITE setPivotA NOTIFY pivotAChanged)
    Q_PROPERTY(QVector3D pivotB READ pivotB WRITE setPivotB NOTIFY pivotBChanged)
    Q_PROPERTY(qreal impulseClamp READ impulseClamp WRITE setImpulseClamp NOTIFY impulseClampChanged)
    Q_PROPERTY(qreal tau READ tau WRITE setTau NOTIFY tauChanged)
    Q_PROPERTY(qreal damping READ damping WRITE setDamping NOTIFY dampingChanged)
public:
    explicit Point2PointConstraint(QObject *parent = nullptr);

    QSharedPointer<btPoint2PointConstraint> p2p() const;

    QVector3D pivotA() const;

    QVector3D pivotB() const;

    qreal impulseClamp() const;

    qreal tau() const;

    qreal damping() const;

signals:

    void pivotAChanged(QVector3D pivotA);

    void pivotBChanged(QVector3D pivotB);

    void impulseClampChanged(qreal impulseClamp);

    void tauChanged(qreal tau);

    void dampingChanged(qreal damping);

public slots:

    void setPivotA(QVector3D pivotA);

    void setPivotB(QVector3D pivotB);

    void setImpulseClamp(qreal impulseClamp);

    void setTau(qreal tau);

    void setDamping(qreal damping);

private:
    void postCreate() override;

    QSharedPointer<btTypedConstraint> create() const override;

private:

    QVector3D m_pivotA;
    QVector3D m_pivotB;
    qreal m_impulseClamp;
    qreal m_tau;
    qreal m_damping;
};

}

#endif // POINT2POINTCONSTRAINT_H
