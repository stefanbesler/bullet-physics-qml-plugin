/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "CollisionShape.h"
#include "QBullet.h"

namespace QBullet {

CollisionShape::~CollisionShape()
{
}

QSharedPointer<btCollisionShape> CollisionShape::shape() const
{
    return m_btShape;
}

void CollisionShape::clear()
{
    if(m_btShape.isNull())
        return;
    m_btShape.clear();
    postClear();
    emit shapeReset();
    emit nameChanged(name());
}

void CollisionShape::postClear()
{

}

void CollisionShape::preCreate()
{

}

void CollisionShape::postCreate()
{
    if(m_btShape) {
        if(m_scaleSet) {
            m_btShape->setLocalScaling(q2b(m_scale));
        } else {
            m_scale = b2q(m_btShape->getLocalScaling());
            emit scaleChanged(m_scale);
        }
    }
}

void CollisionShape::resetShape()
{
    if(!isComplete())
        return;
    preCreate();
    m_btShape = create();
    postCreate();
    emit shapeReset();
    emit nameChanged(name());

}

void CollisionShape::onComponentComplete()
{
    resetShape();
}

void CollisionShape::setPropertyChanged()
{
    emit propertyChanged();
}

QVector3D CollisionShape::scale() const
{
    return m_scale;
}

QString CollisionShape::name() const
{
    return m_btShape?QString::fromLatin1(m_btShape->getName()):QString("unknown");
}

void CollisionShape::setScale(QVector3D localScaling)
{
    if (m_scale == localScaling)
        return;

    m_scale = localScaling;
    m_scaleSet = true;
    if(shape()) {
        shape()->setLocalScaling(q2b(m_scale));
    }
    emit scaleChanged(m_scale);
}

CollisionShape::CollisionShape(QObject *parent)
    : BulletObject(parent)
    , m_scale(1.0, 1.0, 1.0)
    , m_scaleSet(false)
{

}

}

#include "moc_CollisionShape.cpp"
