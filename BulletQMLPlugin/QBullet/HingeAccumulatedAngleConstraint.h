/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef HINGEACCUMULATEDANGLECONSTRAINT_H
#define HINGEACCUMULATEDANGLECONSTRAINT_H

#include "HingeConstraint.h"

class btHingeAccumulatedAngleConstraint;

namespace QBullet
{

class HingeAccumulatedAngleConstraint: public HingeConstraint
{
    Q_OBJECT
    Q_PROPERTY(qreal accumulatedHingeAngle READ accumulatedHingeAngle WRITE setAccumulatedHingeAngle NOTIFY accumulatedHingeAngleChanged)
public:
    HingeAccumulatedAngleConstraint(QObject *parent = nullptr);

    qreal accumulatedHingeAngle() const;

    QSharedPointer<btHingeAccumulatedAngleConstraint> hingeAA() const;

public slots:
    void setAccumulatedHingeAngle(qreal accumulatedHingeAngle);

signals:
    void accumulatedHingeAngleChanged(qreal accumulatedHingeAngle);

protected:
    void postCreate() override;

    btHingeConstraint *create(btRigidBody& rbA, btRigidBody& rbB, const btVector3& pivotInA,const btVector3& pivotInB, const btVector3& axisInA,const btVector3& axisInB, bool useReferenceFrameA = false) const override;

    btHingeConstraint *create(btRigidBody& rbA,const btVector3& pivotInA,const btVector3& axisInA, bool useReferenceFrameA = false) const override;

    btHingeConstraint *create(btRigidBody& rbA,btRigidBody& rbB, const btTransform& rbAFrame, const btTransform& rbBFrame, bool useReferenceFrameA = false) const override;

    btHingeConstraint *create(btRigidBody& rbA,const btTransform& rbAFrame, bool useReferenceFrameA = false) const override;

private:

    qreal m_accumulatedHingeAngle;
};

}

#endif // HINGEACCUMULATEDANGLECONSTRAINT_H
