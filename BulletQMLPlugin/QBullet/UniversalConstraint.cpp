/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "UniversalConstraint.h"
#include "QBullet.h"

namespace QBullet
{

UniversalConstraint::UniversalConstraint(QObject *parent)
    : Generic6DofSpringConstraint(parent)
    , m_axisA(0, 0, 1)
    , m_axisB(0, 0, 1)
{

}

QVector3D UniversalConstraint::anchor() const
{
    return m_anchor;
}

QVector3D UniversalConstraint::axisA() const
{
    return m_axisA;
}

QVector3D UniversalConstraint::axisB() const
{
    return m_axisB;
}

void UniversalConstraint::setAnchor(QVector3D anchor)
{
    if (m_anchor == anchor)
        return;

    m_anchor = anchor;
    resetConstraint();
    emit anchorChanged(m_anchor);
}

void UniversalConstraint::setAxisA(QVector3D axis1)
{
    if (m_axisA == axis1)
        return;

    m_axisA = axis1;
    if(universal()) {
        universal()->setAxis(q2b(m_axisA), q2b(m_axisB));
    }
    emit axisAChanged(m_axisA);
}

void UniversalConstraint::setAxisB(QVector3D axis2)
{
    if (m_axisB == axis2)
        return;

    m_axisB = axis2;
    if(universal()) {
        universal()->setAxis(q2b(m_axisA), q2b(m_axisB));
    }
    emit axisBChanged(m_axisB);
}

QSharedPointer<btTypedConstraint> UniversalConstraint::create() const
{
    if(rbA()&&rbB()) {
        btVector3 anchor(q2b(m_anchor)), axis1(q2b(m_axisA)), axis2(q2b(m_axisB));
        return QSharedPointer<btTypedConstraint>(new btUniversalConstraint(*rbA().data(),
                                                                           *rbB().data(),
                                                                           anchor,
                                                                           axis1,
                                                                           axis2));

    }
    return QSharedPointer<btTypedConstraint>(0);
}

QSharedPointer<btUniversalConstraint> UniversalConstraint::universal() const
{
    return constraint().dynamicCast<btUniversalConstraint>();
}

}
