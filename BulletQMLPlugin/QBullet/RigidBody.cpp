/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QtDebug>
#include <btBulletDynamicsCommon.h>
#include "CollisionShape.h"
#include "DiscreteDynamicsWorld.h"
#include "QBullet.h"
#include "RigidBody.h"

namespace QBullet {

class MotionState: public btDefaultMotionState
{
public:
    MotionState(RigidBody *rigidBody, const btTransform& startTrans = btTransform::getIdentity(),const btTransform& centerOfMassOffset = btTransform::getIdentity())
        : btDefaultMotionState(startTrans, centerOfMassOffset)
        , m_rigidBody(rigidBody)
    {

    }

    void setWorldTransform(const btTransform& centerOfMassWorldTrans) override
    {
        btDefaultMotionState::setWorldTransform(centerOfMassWorldTrans);
        m_rigidBody->updateFromBulletEngine(centerOfMassWorldTrans);
    }

private:
    RigidBody *m_rigidBody;
};

RigidBody::RigidBody(QObject *parent)
    : CollisionObject(parent)
    , m_mass(0)
    , m_gravitySet(false)
    , m_anisotropicFrictionSet(false)
    , m_linearDamping(0)
    , m_angularDamping(0)
    , m_restitution(0)
    , m_friction(0.5)//From default value btRigidBodyConstructionInfo.
    , m_rollingFriction(0)
    , m_spinningFriction(0)
{

}

RigidBody::~RigidBody()
{
    if(body()) {
        takeFromWorld();
        delete body()->getMotionState();
    }
}

QSharedPointer<btRigidBody> RigidBody::body() const
{
    return collisionObject().dynamicCast<btRigidBody>();
}

qreal RigidBody::mass() const
{
    return m_mass;
}

QVector3D RigidBody::linearVelocity() const
{
    return m_linearVelocity;
}

QVector3D RigidBody::angularVelocity() const
{
    return m_angularVelocity;
}

QVector3D RigidBody::gravity() const
{
    return m_gravity;
}

qreal RigidBody::restitution() const
{
    return m_restitution;
}

qreal RigidBody::friction() const
{
    return m_friction;
}

qreal RigidBody::rollingFriction() const
{
    return m_rollingFriction;
}

QVector3D RigidBody::anisotropicFriction() const
{
    return m_anisotropicFriction;
}

qreal RigidBody::linearDamping() const
{
    return m_linearDamping;
}

qreal RigidBody::angularDamping() const
{
    return m_angularDamping;
}

qreal RigidBody::spinningFriction() const
{
    return m_spinningFriction;
}

void RigidBody::applyForce(const QVector3D &force, const QVector3D &localPoint)
{
    if(body().isNull())
        return;
    body()->applyForce(q2b(force), q2b(localPoint));
}

void RigidBody::applyCentralForce(const QVector3D &force)
{
    if(body().isNull())
        return;
    body()->applyCentralForce(q2b(force));
}

void RigidBody::applyCentralImpulse(const QVector3D &force)
{
    if(body().isNull())
        return;
    body()->applyCentralImpulse(q2b(force));
}

void RigidBody::applyTorque(const QVector3D &torque)
{
    if(body().isNull())
        return;
    body()->applyTorque(q2b(torque));
}

void RigidBody::applyTorqueImpulse(const QVector3D &torque)
{
    if(body().isNull())
        return;
    body()->applyTorqueImpulse(q2b(torque));
}

void RigidBody::clearForces()
{
    if(body().isNull())
        return;
    body()->clearForces();
}

void RigidBody::setMass(qreal mass)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_mass, mass))
        return;

    m_mass = mass;
    updateMassProps();
    emit massChanged(m_mass);
}

void RigidBody::onOriginChanged(const QVector3D &origin)
{
    if(body()) {
        //qDebug()<<"RigidBody::onOriginChanged()"<<origin;
        btTransform trans = body()->getWorldTransform();
        trans.setOrigin(q2b(origin));
        /*
        The MotionState will call RigidBody::setTransform() which will emit
        originChanged signal.
        */
        body()->getMotionState()->setWorldTransform(trans);
    }
}

void RigidBody::setLinearVelocity(QVector3D linearVelocity)
{
    if (m_linearVelocity == linearVelocity)
        return;

    m_linearVelocity = linearVelocity;
    if(body())
        body()->setLinearVelocity(q2b(m_linearVelocity));
    emit linearVelocityChanged(m_linearVelocity);
}

void RigidBody::setAngularVelocity(QVector3D angularVelocity)
{
    if (m_angularVelocity == angularVelocity)
        return;
    m_angularVelocity = angularVelocity;
    if(body())
        body()->setAngularVelocity(q2b(m_angularVelocity));
    emit angularVelocityChanged(m_angularVelocity);
}

void RigidBody::setGravity(QVector3D gravity)
{
    if (m_gravity == gravity&&m_gravitySet)
        return;
    m_gravity = gravity;
    m_gravitySet = true;

    if(body())
        body()->setGravity(q2b(m_gravity));
    emit gravityChanged(m_gravity);
}

void RigidBody::onRotationChanged(const QQuaternion &rotation)
{
    if(body()) {
        btTransform trans = body()->getWorldTransform();
        trans.setRotation(q2b(rotation));
        /*
        The MotionState will call RigidBody::setTransform() which will emit
        originChanged signal.
        */
        body()->getMotionState()->setWorldTransform(trans);
    }
}

void RigidBody::setRestitution(qreal restitution)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_restitution, restitution))
        return;
    m_restitution = restitution;
    if(body())
        body()->setRestitution(m_restitution);
    emit restitutionChanged(m_restitution);
}

void RigidBody::setFriction(qreal friction)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_friction, friction))
        return;

    m_friction = friction;
    if(body())
        body()->setFriction(m_friction);
    emit frictionChanged(m_friction);
}

void RigidBody::setRollingFriction(qreal rollingFriction)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_rollingFriction, rollingFriction))
        return;
    m_rollingFriction = rollingFriction;

    if(body())
        body()->setRollingFriction(m_rollingFriction);
    emit rollingFrictionChanged(m_rollingFriction);
}

void RigidBody::setAnisotropicFriction(QVector3D anisotropicFriction)
{
    if (m_anisotropicFriction == anisotropicFriction)
        return;

    m_anisotropicFrictionSet = true;
    m_anisotropicFriction = anisotropicFriction;
    if(body())
        body()->setAnisotropicFriction(q2b(anisotropicFriction));
    emit anisotropicFrictionChanged(anisotropicFriction);
}

void RigidBody::setLinearDamping(qreal linearDamping)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_linearDamping, linearDamping))
        return;

    m_linearDamping = linearDamping;
    if(body())
        body()->setDamping(m_linearDamping, m_angularDamping);
    emit linearDampingChanged(m_linearDamping);
}

void RigidBody::setAngularDamping(qreal angularDamping)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_angularDamping, angularDamping))
        return;

    m_angularDamping = angularDamping;
    if(body())
        body()->setDamping(m_linearDamping, m_angularDamping);
    emit angularDampingChanged(m_angularDamping);
}

void RigidBody::setSpinningFriction(qreal spinningFriction)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_spinningFriction, spinningFriction))
        return;

    m_spinningFriction = spinningFriction;
    if(body())
        body()->setSpinningFriction(m_spinningFriction);
    emit spinningFrictionChanged(m_spinningFriction);
}

void RigidBody::onCollisionShapePropertyChanged()
{
    takeFromWorld();
    updateMassProps();
    addToWorld();
}

void RigidBody::onAddToWorld(btDiscreteDynamicsWorld *world, bool hasFilterSet, int collisionFilterGroup, int collisionFilterMask)
{
    if(body().isNull())
        return;
    if(hasFilterSet) {
        world->addRigidBody(body().data(), collisionFilterGroup, collisionFilterMask);
    } else {
        world->addRigidBody(body().data());
    }
    //        There are five states a rigid body could be in:

    //        ACTIVE_TAG
    //        Means active so that the object having the state could be moved in a step simulation. This is the "normal" state for an object to be in. Use btCollisionObject::activate() to activate an object, not btCollisionObject::setActivationState(ACTIVATE_TAG), or it may get disabled again right away, as the deactivation timer has not been reset.
    //        DISABLE_DEACTIVATION
    //        Makes a body active forever, used for something like a player-controlled object.
    //        DISABLE_SIMULATION
    //        Does the opposite, making a body deactivated forever.
    //        ISLAND_SLEEPING
    //        Means the body, and it's island, are asleep, since Bullet sleeps objects per-island. You probably don't want or need to set this one manually.
    //        WANTS_DEACTIVATION
    //        Means that it's an active object trying to fall asleep, and Bullet is keeping an eye on its velocity for the next few frames to see if it's a good candidate. You probably don't want or need to set this one manually.
    if(!body()->isStaticObject()) {
        body()->setActivationState(DISABLE_DEACTIVATION);
    } else {
        body()->forceActivationState(WANTS_DEACTIVATION);
    }
}

void RigidBody::onWorldGravityChanged()
{
    updateMassProps();
}

void RigidBody::onCollisionShapeReset()
{
    updateMassProps();
}

void RigidBody::onWorldChanged()
{
    updateMassProps();
}

QSharedPointer<btCollisionObject> RigidBody::create() const
{
    MotionState* motionState = new MotionState(const_cast<RigidBody*>(this),
                                               btTransform(q2b(rotation()),
                                                           q2b(origin())));
    btVector3 inertia(0, 0, 0);
    if(bulletCollisionShape()) {
        bulletCollisionShape()->calculateLocalInertia(m_mass, inertia);
    }
    btRigidBody::btRigidBodyConstructionInfo ci(m_mass, motionState, bulletCollisionShape().data(), inertia);
    ci.m_linearDamping = m_linearDamping;
    ci.m_angularDamping = m_angularDamping;
    ci.m_friction = m_friction;
    ci.m_rollingFriction = m_rollingFriction;
    ci.m_restitution = m_restitution;
    ci.m_spinningFriction = m_spinningFriction;

    return QSharedPointer<btCollisionObject>(new btRigidBody(ci));
}

void RigidBody::postCreate()
{
    CollisionObject::postCreate();
    if(body().isNull())
        return;
    Q_ASSERT(body()->getMotionState());

    if(m_gravitySet) {
        body()->setGravity(q2b(m_gravity));
    } else {
        m_gravity = b2q(body()->getGravity());
        emit gravityChanged(m_gravity);
    }
    if(m_anisotropicFrictionSet) {
        body()->setAnisotropicFriction(q2b(m_anisotropicFriction));
    } else {
        m_anisotropicFriction = b2q(body()->getAnisotropicFriction());
        emit anisotropicFrictionChanged(m_anisotropicFriction);
    }
    //Initial transform
    btTransform trans;
    trans.setIdentity();
    trans.setOrigin(q2b(origin()));
    trans.setRotation(q2b(rotation()));
    body()->getMotionState()->setWorldTransform(trans);
    //Initial velocity.
    body()->setLinearVelocity(q2b(m_linearVelocity));
    body()->setAngularVelocity(q2b(m_angularVelocity));
}


bool RigidBody::updateFromBulletEngine(const btTransform &mat)
{
    //qDebug()<<"RigidBody::setTransform()";
    if(!CollisionObject::updateFromBulletEngine(mat))
        return false;

    Q_ASSERT(body());
    {
        QVector3D velocity = b2q(body()->getLinearVelocity());
        if(m_linearVelocity!=velocity) {
            m_linearVelocity = velocity;
            emit linearVelocityChanged(m_linearVelocity);
        }
    }
    {
        QVector3D velocity = b2q(body()->getLinearVelocity());
        if(m_angularVelocity!=velocity) {
            m_angularVelocity = velocity;
            emit angularVelocityChanged(m_angularVelocity);
        }
    }
}

void RigidBody::updateMassProps()
{
    if(body().isNull())
        return;
    btVector3 inertia(0, 0, 0);
    if(bulletCollisionShape()) {
        bulletCollisionShape()->calculateLocalInertia(m_mass, inertia);
    }
    body()->setMassProps(m_mass, inertia);
}

}

#include "moc_RigidBody.cpp"
