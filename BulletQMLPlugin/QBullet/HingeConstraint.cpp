/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "HingeConstraint.h"
#include "QBullet.h"

namespace QBullet
{

HingeConstraint::HingeConstraint(QObject *parent)
    : Constraint(parent)
    , m_low()
    , m_high(-1)
    , m_softness(0.9)
    , m_biasFactor(0.3)
    , m_relaxationFactor(1.0)
{

}


QVector3D HingeConstraint::pivotA() const
{
    return m_pivotA;
}

QVector3D HingeConstraint::pivotB() const
{
    return m_pivotB;
}

QVector3D HingeConstraint::axisA() const
{
    return m_axisA;
}

QVector3D HingeConstraint::axisB() const
{
    return m_axisB;
}

qreal HingeConstraint::low() const
{
    return m_low;
}

qreal HingeConstraint::high() const
{
    return m_high;
}

qreal HingeConstraint::softness() const
{
    return m_softness;
}

qreal HingeConstraint::biasFactor() const
{
    return m_biasFactor;
}

qreal HingeConstraint::relaxationFactor() const
{
    return m_relaxationFactor;
}

bool HingeConstraint::hasLimit() const
{
    return hinge()?hinge()->hasLimit():false;
}

void HingeConstraint::setPivotA(QVector3D pivotA)
{
    if (m_pivotA == pivotA)
        return;
    m_pivotA = pivotA;
    if(!m_axisA.isNull()&&!m_axisB.isNull()) {
        resetConstraint();
    }
    emit pivotAChanged(m_pivotA);
}

void HingeConstraint::setPivotB(QVector3D pivotInB)
{
    if (m_pivotB == pivotInB)
        return;

    m_pivotB = pivotInB;
    if(!m_axisA.isNull()&&!m_axisB.isNull()) {
        resetConstraint();
    }
    emit pivotBChanged(m_pivotB);
}

void HingeConstraint::setAxisA(QVector3D axisA)
{
    if (m_axisA == axisA)
        return;

    m_axisA = axisA;
    if(hinge()) {
        btVector3 tmp = q2b(axisA);
        hinge()->setAxis(tmp);
    } else {
        resetConstraint();
    }
    emit axisAChanged(m_axisA);
}

void HingeConstraint::setAxisB(QVector3D axisB)
{
    if (m_axisB == axisB)
        return;
    m_axisB = axisB;
    resetConstraint();
    emit axisBChanged(m_axisB);
}

void HingeConstraint::setLow(qreal low)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_low, low))
        return;

    m_low = low;
    resetConstraint();
    emit lowChanged(m_low);
}

void HingeConstraint::setHigh(qreal high)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_high, high))
        return;

    m_high = high;
    resetConstraint();
    emit highChanged(m_high);
}

void HingeConstraint::setSoftness(qreal softness)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_softness, softness))
        return;

    m_softness = softness;
    resetConstraint();
    emit softnessChanged(m_softness);
}

void HingeConstraint::setBiasFactor(qreal biasFactor)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_biasFactor, biasFactor))
        return;

    m_biasFactor = biasFactor;
    resetConstraint();
    emit biasFactorChanged(m_biasFactor);
}

void HingeConstraint::setRelaxationFactor(qreal relaxationFactor)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_relaxationFactor, relaxationFactor))
        return;

    m_relaxationFactor = relaxationFactor;
    emit relaxationFactorChanged(m_relaxationFactor);
}

void HingeConstraint::setTransformA(QMatrix4x4 transformA)
{
    if (m_transformA == transformA)
        return;

    m_transformA = transformA;
    if(hinge()) {
        hinge()->setFrames(q2b(m_transformA), q2b(m_transformB));
    }
    emit transformAChanged(m_transformA);
}

void HingeConstraint::setTransformB(QMatrix4x4 transformB)
{
    if (m_transformB == transformB)
        return;

    m_transformB = transformB;
    if(hinge()) {
        hinge()->setFrames(q2b(m_transformA), q2b(m_transformB));
    }
    emit transformBChanged(m_transformB);
}

QSharedPointer<btTypedConstraint> HingeConstraint::create() const
{
    QSharedPointer<btTypedConstraint> hinge;
    if(rbA()) {
        if(rbB()) {
            if(m_axisA.isNull()||m_axisB.isNull()) {
                hinge = QSharedPointer<btTypedConstraint>(create(*rbA().data(), *rbB().data(), q2b(m_transformA), q2b(m_transformB)));
            } else {
                hinge = QSharedPointer<btTypedConstraint>(create(*rbA().data(), *rbB().data(), q2b(m_pivotA), q2b(m_pivotB), q2b(m_axisA), q2b(m_axisB)));
            }
        } else {
            if(m_axisA.isNull()) {
                hinge = QSharedPointer<btTypedConstraint>(create(*rbA().data(), q2b(m_transformA)));
            } else {
                hinge = QSharedPointer<btTypedConstraint>(create(*rbA().data(), q2b(m_pivotA), q2b(m_axisA)));
            }
        }
    }
    return hinge;
}

void HingeConstraint::postCreate()
{
    if(this->hinge()) {
        this->hinge()->setLimit(qDegreesToRadians(m_low), qDegreesToRadians(m_high), m_softness, m_biasFactor, m_relaxationFactor);
    }
}

btHingeConstraint *HingeConstraint::create(btRigidBody &rbA, btRigidBody &rbB, const btVector3 &pivotInA, const btVector3 &pivotInB, const btVector3 &axisInA, const btVector3 &axisInB, bool useReferenceFrameA) const
{
    return new btHingeConstraint(rbA, rbB, pivotInA, pivotInB, axisInA, axisInB, useReferenceFrameA);
}

btHingeConstraint *HingeConstraint::create(btRigidBody &rbA, const btVector3 &pivotInA, const btVector3 &axisInA, bool useReferenceFrameA) const
{
    return new btHingeConstraint(rbA, pivotInA, axisInA, useReferenceFrameA);
}

btHingeConstraint *HingeConstraint::create(btRigidBody &rbA, btRigidBody &rbB, const btTransform &rbAFrame, const btTransform &rbBFrame, bool useReferenceFrameA) const
{
    return new btHingeConstraint(rbA, rbB, rbAFrame, rbBFrame, useReferenceFrameA);
}

btHingeConstraint *HingeConstraint::create(btRigidBody &rbA, const btTransform &rbAFrame, bool useReferenceFrameA) const
{
    return new btHingeConstraint(rbA, rbAFrame, useReferenceFrameA);
}

QSharedPointer<btHingeConstraint> HingeConstraint::hinge() const
{
    return constraint().dynamicCast<btHingeConstraint>();
}

QMatrix4x4 HingeConstraint::transformA() const
{
    return m_transformA;
}

QMatrix4x4 HingeConstraint::transformB() const
{
    return m_transformB;
}
}
