/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "DiscreteDynamicsWorld.h"
#include "QBullet.h"
#include "RayTest.h"
#include "RigidBody.h"

namespace QBullet
{
RayTest::RayTest(QObject *parent)
    : QObject(parent)
    , m_world(0)
    , m_hitBody(0)
    , m_hasHit(false)
    , m_collisionFilterSet(false)
    , m_collisionFilterGroup(0)
    , m_collisionFilterMask(0)
{

}

DiscreteDynamicsWorld *RayTest::world() const
{
    return m_world;
}

RigidBody *RayTest::hitBody() const
{
    return m_hitBody;
}

QVector3D RayTest::rayFrom() const
{
    return m_rayFrom;
}

QVector3D RayTest::rayTo() const
{
    return m_rayTo;
}

QVector3D RayTest::hitPosition() const
{
    return m_hitPosition;
}

QVector3D RayTest::rayVector() const
{
    return m_rayTo - m_rayFrom;
}

bool RayTest::hasHit() const
{
    return m_hasHit;
}

int RayTest::collisionFilterGroup() const
{
    return m_collisionFilterGroup;
}

int RayTest::collisionFilterMask() const
{
    return m_collisionFilterMask;
}

void RayTest::setWorld(DiscreteDynamicsWorld *world)
{
    if (m_world == world)
        return;
    clear();
    m_worldData.clear();
    m_world = world;
    if(m_world) {
        m_worldData = m_world->worldData();
    }
    emit worldChanged(m_world);
}

void RayTest::hitTest()
{
    clear();

    if(m_worldData.isNull())
        return;
    if(m_rayFrom==m_rayTo)
        return;

    btCollisionWorld::ClosestRayResultCallback rayCallback(q2b(m_rayFrom), q2b(m_rayTo));
    if(m_collisionFilterSet) {
        rayCallback.m_collisionFilterGroup = m_collisionFilterGroup;
        rayCallback.m_collisionFilterMask = m_collisionFilterMask;
    }
    m_worldData->dynamicsWorld->rayTest(q2b(m_rayFrom), q2b(m_rayTo), rayCallback);
    m_hasHit = rayCallback.hasHit();
    if (rayCallback.hasHit()) {
        btVector3 pickPos = rayCallback.m_hitPointWorld;
        m_hitPosition = b2q(pickPos);
        btRigidBody* body = (btRigidBody*)btRigidBody::upcast(rayCallback.m_collisionObject);
        if (body) {
            //if (!(body->isStaticObject() || body->isKinematicObject())) {
                m_hitBody = (RigidBody *)body->getUserPointer();
                if(m_hitBody) {
                    connect(m_hitBody, &RigidBody::destroyed, this, &RayTest::clear);
                }

                emit hitBodyChanged(m_hitBody);
            //}
        }
        emit hitPositionChanged(m_hitPosition);
        emit hasHitChanged(hasHit());
        //m_oldPickingPos = rayToWorld;

        //m_oldPickingDist = (pickPos - rayFromWorld).length();
    }
}

void RayTest::clear()
{
    m_hasHit = false;
    m_hitPosition = QVector3D();
    if(m_hitBody) {
        m_hitBody->disconnect(this);
        m_hitBody = 0;
        emit hitBodyChanged(0);
    }
    emit hitPositionChanged(m_hitPosition);
    emit hasHitChanged(hasHit());
}

void RayTest::setRayFrom(QVector3D rayFrom)
{
    if (m_rayFrom == rayFrom)
        return;

    m_rayFrom = rayFrom;
    emit rayFromChanged(m_rayFrom);
    emit rayVectorChanged(rayVector());
}

void RayTest::setRayTo(QVector3D rayTo)
{
    if (m_rayTo == rayTo)
        return;

    m_rayTo = rayTo;
    emit rayToChanged(m_rayTo);
    emit rayVectorChanged(rayVector());
}

void RayTest::setRayVector(QVector3D rayVector)
{
    if (this->rayVector() == rayVector)
        return;

    setRayTo(m_rayFrom + rayVector);
}

void RayTest::setCollisionFilterGroup(int group)
{
    if (m_collisionFilterGroup == group)
        return;
    m_collisionFilterSet = true;
    m_collisionFilterGroup = group;
    emit collisionFilterGroupChanged(m_collisionFilterGroup);
}

void RayTest::setCollisionFilterMask(int mask)
{
    if (m_collisionFilterMask == mask)
        return;
    m_collisionFilterSet = true;
    m_collisionFilterMask = mask;
    emit collisionFilterMaskChanged(m_collisionFilterMask);
}

}
