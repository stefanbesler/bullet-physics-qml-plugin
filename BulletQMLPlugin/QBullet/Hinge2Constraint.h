/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef HINGE2CONSTRAINT_H
#define HINGE2CONSTRAINT_H

#include <QVector3D>
#include "Generic6DofSpring2Constraint.h"

class btHinge2Constraint;

namespace QBullet
{

/*!
\class Hinge2Constraint Hinge2Constraint.h <QBullet/Hinge2Constraint.h>

*/
class Hinge2Constraint : public Generic6DofSpring2Constraint
{
    Q_OBJECT
    Q_PROPERTY(QVector3D anchor READ anchor WRITE setAnchor NOTIFY anchorChanged)
    Q_PROPERTY(QVector3D axisA READ axisA WRITE setAxisA NOTIFY axisAChanged)
    Q_PROPERTY(QVector3D axisB READ axisB WRITE setAxisB NOTIFY axisBChanged)
public:
    explicit Hinge2Constraint(QObject *parent = nullptr);

    QVector3D anchor() const;

    QVector3D axisA() const;

    QVector3D axisB() const;

    QSharedPointer<btHinge2Constraint> hinge2() const;
signals:

    void anchorChanged(QVector3D anchor);

    void axisAChanged(QVector3D axisA);

    void axisBChanged(QVector3D axisB);

public slots:

    void setAnchor(QVector3D anchor);

    void setAxisA(QVector3D axisA);

    void setAxisB(QVector3D axisB);

protected:
    QSharedPointer<btTypedConstraint> create() const override;


private:
    QVector3D m_anchor;

    QVector3D m_axisA;

    QVector3D m_axisB;
};

}
#endif // HINGE2CONSTRAINT_H
