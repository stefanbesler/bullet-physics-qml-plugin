/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QtDebug>

#include "CollisionObject.h"
#include "ContactCallback.h"
#include "DiscreteDynamicsWorld.h"

namespace QBullet {

ContactCallback::ContactCallback(QObject *parent)
    : BulletObject(parent)
    , m_objectA(0)
    , m_objectB(0)
{

}

ContactCallback::~ContactCallback()
{
    if(m_objectA) {
        m_objectA->removeCallback(this);
    }
}

bool ContactCallback::onContactProcessedCallback(btManifoldPoint &cp,
                                                 CollisionObject *objA,
                                                 CollisionObject *objB)
{
    Q_UNUSED(cp);
    Q_UNUSED(objA);
    Q_UNUSED(objB);
    qDebug()<<"ContactCallback::onContactProcessedCallback() not implemented!";
    return true;
}

bool ContactCallback::onContactAddedCallback(btManifoldPoint &cp,
                                             const CollisionObject *objA,
                                             int partId0,
                                             int index0,
                                             const CollisionObject *objB,
                                             int partId1,
                                             int index1)
{
    Q_UNUSED(cp);
    Q_UNUSED(objA);
    Q_UNUSED(objB);
    Q_UNUSED(partId0);
    Q_UNUSED(index0);
    Q_UNUSED(partId1);
    Q_UNUSED(index1);
    return true;
}

CollisionObject *ContactCallback::object0() const
{
    return m_objectA;
}

CollisionObject *ContactCallback::object1() const
{
    return m_objectB;
}

void ContactCallback::setObject0(CollisionObject *objectA)
{
    if (m_objectA == objectA)
        return;
    if(m_objectB) {
        m_objectB->removeCallback(this);
    }
    if(m_objectA) {
        m_objectA->disconnect(this);
        m_objectA->removeCallback(this);
    }
    m_objectA = objectA;
    if(m_objectA) {
        connect(m_objectA, &CollisionObject::destroyed, this, &ContactCallback::objectDestroyed);
    }
    resetCallback();
    emit object0Changed(m_objectA);
}

void ContactCallback::setObject1(CollisionObject *objectB)
{
    if (m_objectB == objectB)
        return;

    if(m_objectA) {
        m_objectA->removeCallback(this);
    }
    if(m_objectB) {
        m_objectB->disconnect(this);
    }
    m_objectB = objectB;
    resetCallback();
    if(m_objectB) {
        connect(m_objectB, &CollisionObject::destroyed, this, &ContactCallback::objectDestroyed);
    }
    emit object1Changed(m_objectB);
}

void ContactCallback::resetCallback()
{
    if(m_objectA) {
        m_objectA->addCallback(this);
    } else if(m_objectB) {
        m_objectB->addCallback(this);
    }
}

void ContactCallback::objectDestroyed()
{
    if(m_objectA==sender()) {
        m_objectA = 0;
    }
    if(m_objectB==sender()) {
        if(m_objectA) {
            m_objectA->removeCallback(this);
        }
        m_objectB = 0;
    }
    resetCallback();
}

}
