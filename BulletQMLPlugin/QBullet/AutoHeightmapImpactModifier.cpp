/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QtDebug>
#include <btBulletDynamicsCommon.h>

#include "CollisionObject.h"
#include "SphereShape.h"
#include "Heightmap.h"
#include "HeightfieldTerrainShape.h"
#include "AutoHeightmapImpactModifier.h"
#include "QBullet.h"
#include "RigidBody.h"
namespace QBullet {

AutoHeightmapImpactModifier::AutoHeightmapImpactModifier(QObject *parent)
    : ContactCallback(parent)
    , m_modifierScale(0.001)
    , m_impulsiveThreshold(1.0)
{

}

bool AutoHeightmapImpactModifier::onContactProcessedCallback(btManifoldPoint &cp,
                                                   CollisionObject *objA,
                                                   CollisionObject *objB)
{
    Q_ASSERT(objA&&objB);
    HeightfieldTerrainShape *heightfieldShape = qobject_cast<HeightfieldTerrainShape *>(objA->collisionShape());
    QVector3D normalOnB = b2q(cp.m_normalWorldOnB);
    CollisionObject *heightmapObject = objA;
    CollisionObject *otherObject = objB;
    CollisionShape *otherShape = objB->collisionShape();
    QVector3D positionOnA = b2q(cp.getPositionWorldOnA());
    if(heightfieldShape==0) {
        heightfieldShape = qobject_cast<HeightfieldTerrainShape *>(objB->collisionShape());
        otherShape = objA->collisionShape();
        normalOnB = -normalOnB;
        otherObject = objA;
        heightmapObject = objB;
        positionOnA = b2q(cp.getPositionWorldOnB());
    }
    Q_ASSERT(otherShape);
    if(heightfieldShape==0)
        return false;//returned value not being used right now.
    Heightmap *heightmapData = heightfieldShape->heightmap();
    if(heightmapData==0)return false;


    SphereShape *sphere = qobject_cast<SphereShape *>(otherShape);
    if(sphere) {
        //qDebug()<<cp.m_appliedImpulse<<normalOnB;
        if(cp.m_appliedImpulse>m_impulsiveThreshold) {
            QVector3D offset = QVector3D(0, heightmapData->halfHeightValue(), 0) + cp.m_appliedImpulse*normalOnB*m_modifierScale;
            heightmapData->substract(offset + otherObject->origin(),
                                 sphere->radius());
        }
    } else {
        //Other
        if(cp.m_appliedImpulse>m_impulsiveThreshold)
        {
            QVector3D offset = QVector3D(0, heightmapData->halfHeightValue(), 0);
            QVector3D localPosA = heightmapObject->world2Local(positionOnA);
            int row, column;
            QVector3D normalOnA(-normalOnB);
            if(heightmapData->find(localPosA + offset, row, column)) {
                normalOnA = heightmapObject->rotation() * heightmapData->normal(row, column);
            }
            qreal factor = cp.m_appliedImpulse * m_modifierScale;
            if(factor>0.5)factor = 0.5;
            QVector3D moveVector = factor *-normalOnA;

            QVector3D aabbmin, aabbmax;
            otherObject->aabb(aabbmin, aabbmax);
            aabbmin = heightmapObject->world2Local(aabbmin);
            aabbmax = heightmapObject->world2Local(aabbmax);
            QRect rect = heightmapData->toHeightmapRectXZ(aabbmin, aabbmax);
            //qDebug()<<"start update"<<rect;
            for(int row=rect.top(); row<=rect.bottom(); row++) {
                for(int column=rect.left(); column<=rect.right(); column++) {
                    int index = heightmapData->index(row, column);

                    QVector3D vertex = heightmapData->vertex(index);
                    //Need to offset the vertex to the heightmap body space.
                    QVector3D newVertex = heightmapObject->local2World(vertex - offset);

                    //qDebug()<<"to update vertex 1 "<<vertex<<newVertex<<moveVector;

                    if(!otherObject->findClosestPoint(newVertex, QVector3D(0, 1, 0), newVertex)) {
                        //qDebug()<<" no found "<<offset;
                        continue;//no collision
                    }
                    //Move the vertex and map to heightmap body space.


                    newVertex = heightmapObject->world2Local(newVertex + moveVector);
                    //Convert to heightmap space.
                    newVertex+= offset;
                    //qDebug()<<"to update vertex 0 "<<vertex<<newVertex<<moveVector;
                    int tr, tc;
                    if(!heightmapData->find(newVertex, tr, tc)) {
                        continue;
                    }
                    vertex = heightmapData->vertex(tr, tc);
                    //qDebug()<<"to update vertex 1 "<<vertex<<newVertex<<moveVector;
                    if(newVertex.y()>vertex.y())
                        continue;

                    //qDebug()<<"update vertex "<<tr<<tc<<vertex<<newVertex<<moveVector;

                    heightmapData->setVertex(tr, tc, newVertex);

                    if(aabbmin.x()>newVertex.x())
                        aabbmin.setX(newVertex.x());
                    if(aabbmin.z()>newVertex.z())
                        aabbmin.setZ(newVertex.z());

                    if(aabbmax.x()<newVertex.x())
                        aabbmax.setX(newVertex.x());
                    if(aabbmax.z()<newVertex.z())
                        aabbmax.setZ(newVertex.z());
                }
            }

            rect = heightmapData->toHeightmapRectXZ(aabbmin, aabbmax);
            //qDebug()<<"update normal"<<rect;
            for(int row=rect.top(); row<=rect.bottom(); row++) {
                for(int column=rect.left(); column<=rect.right(); column++) {
                    heightmapData->updateNormal(row, column);
                }
            }
            heightmapData->setChanged();
        }
    }

    return true;
}

bool AutoHeightmapImpactModifier::onContactAddedCallback(btManifoldPoint &cp, const CollisionObject *objA, int partId0, int index0, const CollisionObject *objB, int partId1, int index1)
{
    return true;
}

qreal AutoHeightmapImpactModifier::modifierScale() const
{
    return m_modifierScale;
}

qreal AutoHeightmapImpactModifier::impulsiveThreshold() const
{
    return m_impulsiveThreshold;
}

void AutoHeightmapImpactModifier::setModifierScale(qreal modifierScale)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_modifierScale, modifierScale))
        return;

    m_modifierScale = modifierScale;
    emit modifierScaleChanged(m_modifierScale);
}

void AutoHeightmapImpactModifier::setImpulsiveThreshold(qreal impulsiveThreshold)
{
    if(impulsiveThreshold<0)
        return;
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_impulsiveThreshold, impulsiveThreshold))
        return;

    m_impulsiveThreshold = impulsiveThreshold;
    emit impulsiveThresholdChanged(m_impulsiveThreshold);
}

}
