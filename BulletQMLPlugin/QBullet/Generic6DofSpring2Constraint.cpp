/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "Generic6DofSpring2Constraint.h"
#include "QBullet.h"

namespace QBullet
{

Generic6DofSpring2Constraint::Generic6DofSpring2Constraint(QObject *parent)
    : Constraint(parent)
    , m_rotateOrder(RO_XYZ)
    , m_linearSpringXEnabled(false)
    , m_linearSpringYEnabled(false)
    , m_linearSpringZEnabled(false)
    , m_angularSpringXEnabled(false)
    , m_angularSpringYEnabled(false)
    , m_angularSpringZEnabled(false)
    , m_linearMotorXEnabled(false)
    , m_linearMotorYEnabled(false)
    , m_linearMotorZEnabled(false)
    , m_angularMotorXEnabled(false)
    , m_angularMotorYEnabled(false)
    , m_angularMotorZEnabled(false)
    , m_linearServoXEnabled(false)
    , m_linearServoYEnabled(false)
    , m_linearServoZEnabled(false)
    , m_angularServoXEnabled(false)
    , m_angularServoYEnabled(false)
    , m_angularServoZEnabled(false)
{    
}

Generic6DofSpring2Constraint::RotateOrder Generic6DofSpring2Constraint::rotateOrder() const
{
    return m_rotateOrder;
}

void Generic6DofSpring2Constraint::setRotateOrder(Generic6DofSpring2Constraint::RotateOrder rotateOrder)
{
    if (m_rotateOrder == rotateOrder)
        return;

    m_rotateOrder = rotateOrder;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setRotationOrder((::RotateOrder)m_rotateOrder);
    }
    emit rotateOrderChanged(m_rotateOrder);
}

void Generic6DofSpring2Constraint::setLinearSpringXEnabled(bool linearSpringXEnabled)
{
    if (m_linearSpringXEnabled == linearSpringXEnabled)
        return;

    m_linearSpringXEnabled = linearSpringXEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableSpring(0, linearSpringXEnabled);
    }
    emit linearSpringXEnabledChanged(m_linearSpringXEnabled);
}

void Generic6DofSpring2Constraint::setLinearSpringYEnabled(bool linearSpringYEnabled)
{
    if (m_linearSpringYEnabled == linearSpringYEnabled)
        return;

    m_linearSpringYEnabled = linearSpringYEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableSpring(1, linearSpringYEnabled);
    }
    emit linearSpringYEnabledChanged(m_linearSpringYEnabled);
}

void Generic6DofSpring2Constraint::setLinearSpringZEnabled(bool linearSpringZEnabled)
{
    if (m_linearSpringZEnabled == linearSpringZEnabled)
        return;

    m_linearSpringZEnabled = linearSpringZEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableSpring(2, linearSpringZEnabled);
    }
    emit linearSpringZEnabledChanged(m_linearSpringZEnabled);
}

void Generic6DofSpring2Constraint::setAngularSpringXEnabled(bool angularSpringXEnabled)
{
    if (m_angularSpringXEnabled == angularSpringXEnabled)
        return;

    m_angularSpringXEnabled = angularSpringXEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableSpring(3, angularSpringXEnabled);
    }
    emit angularSpringXEnabledChanged(m_angularSpringXEnabled);
}

void Generic6DofSpring2Constraint::setAngularSpringYEnabled(bool angularSpringYEnabled)
{
    if (m_angularSpringYEnabled == angularSpringYEnabled)
        return;

    m_angularSpringYEnabled = angularSpringYEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableSpring(4, angularSpringYEnabled);
    }
    emit angularSpringYEnabledChanged(m_angularSpringYEnabled);
}

void Generic6DofSpring2Constraint::setAngularSpringZEnabled(bool angularSpringZEnabled)
{
    if (m_angularSpringZEnabled == angularSpringZEnabled)
        return;

    m_angularSpringZEnabled = angularSpringZEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableSpring(5, angularSpringZEnabled);
    }
    emit angularSpringZEnabledChanged(m_angularSpringZEnabled);
}

QSharedPointer<btTypedConstraint> Generic6DofSpring2Constraint::create() const
{
    if(rbB()) {
        if(rbA()) {
            return QSharedPointer<btTypedConstraint>(new btGeneric6DofSpring2Constraint(*rbA().data(), *rbB().data(),
                                                                                        q2b(transformA()), q2b(transformB()),
                                                                                        (::RotateOrder)m_rotateOrder));
        } else {
            return QSharedPointer<btTypedConstraint>(new btGeneric6DofSpring2Constraint(*rbB().data(), q2b(transformB()),
                                                                                        (::RotateOrder)m_rotateOrder));
        }
    }
    return QSharedPointer<btTypedConstraint>(0);
}

QSharedPointer<btGeneric6DofSpring2Constraint> Generic6DofSpring2Constraint::generic6DofSpring2() const
{
    return constraint().dynamicCast<btGeneric6DofSpring2Constraint>();
}

bool Generic6DofSpring2Constraint::isLinearSpringXEnabled() const
{
    return m_linearSpringXEnabled;
}

bool Generic6DofSpring2Constraint::isLinearSpringYEnabled() const
{
    return m_linearSpringYEnabled;
}

bool Generic6DofSpring2Constraint::isLinearSpringZEnabled() const
{
    return m_linearSpringZEnabled;
}

bool Generic6DofSpring2Constraint::isAngularSpringXEnabled() const
{
    return m_angularSpringXEnabled;
}

bool Generic6DofSpring2Constraint::isAngularSpringYEnabled() const
{
    return m_angularSpringYEnabled;
}

bool Generic6DofSpring2Constraint::isAngularSpringZEnabled() const
{
    return m_angularSpringZEnabled;
}


QVector3D Generic6DofSpring2Constraint::linearStiffness() const
{
    return m_linearStiffness;
}

QVector3D Generic6DofSpring2Constraint::angularStiffness() const
{
    return m_angularStiffness;
}

QVector3D Generic6DofSpring2Constraint::linearDamping() const
{
    return m_linearDamping;
}

QVector3D Generic6DofSpring2Constraint::angularDamping() const
{
    return m_angularDamping;
}

QVector3D Generic6DofSpring2Constraint::linearBounce() const
{
    return m_linearBounce;
}

QVector3D Generic6DofSpring2Constraint::angularBounce() const
{
    return m_angularBounce;
}

bool Generic6DofSpring2Constraint::isLinearMotorXEnabled() const
{
    return m_linearMotorXEnabled;
}

bool Generic6DofSpring2Constraint::isLinearMotorYEnabled() const
{
    return m_linearMotorYEnabled;
}

bool Generic6DofSpring2Constraint::isLinearMotorZEnabled() const
{
    return m_linearMotorZEnabled;
}

bool Generic6DofSpring2Constraint::isAngularMotorXEnabled() const
{
    return m_angularMotorXEnabled;
}

bool Generic6DofSpring2Constraint::isAngularMotorYEnabled() const
{
    return m_angularMotorYEnabled;
}

bool Generic6DofSpring2Constraint::isAngularMotorZEnabled() const
{
    return m_angularMotorZEnabled;
}

bool Generic6DofSpring2Constraint::isLinearServoXEnabled() const
{
    return m_linearServoXEnabled;
}

bool Generic6DofSpring2Constraint::isLinearServoYEnabled() const
{
    return m_linearServoYEnabled;
}

bool Generic6DofSpring2Constraint::isLinearServoZEnabled() const
{
    return m_linearServoZEnabled;
}

bool Generic6DofSpring2Constraint::isAngularServoXEnabled() const
{
    return m_angularServoXEnabled;
}

bool Generic6DofSpring2Constraint::isAngularServoYEnabled() const
{
    return m_angularServoYEnabled;
}

bool Generic6DofSpring2Constraint::isAngularServoZEnabled() const
{
    return m_angularServoZEnabled;
}

QVector3D Generic6DofSpring2Constraint::linearTargetVelocity() const
{
    return m_linearTargetVelocity;
}

QVector3D Generic6DofSpring2Constraint::angularTargetVelocity() const
{
    return m_angularTargetVelocity;
}

QVector3D Generic6DofSpring2Constraint::linearServoTarget() const
{
    return m_linearServoTarget;
}

QVector3D Generic6DofSpring2Constraint::angularServoTarget() const
{
    return m_angularServoTarget;
}

QVector3D Generic6DofSpring2Constraint::maxLinearMotorForce() const
{
    return m_maxLinearMotorForce;
}

QVector3D Generic6DofSpring2Constraint::maxAngularMotorForce() const
{
    return m_maxAngularMotorForce;
}

QVector3D Generic6DofSpring2Constraint::linearLowerLimit() const
{
    return m_linearLowerLimit;
}

QVector3D Generic6DofSpring2Constraint::linearUpperLimit() const
{
    return m_linearUpperLimit;
}

QVector3D Generic6DofSpring2Constraint::angularLowerLimit() const
{
    return m_angularLowerLimit;
}

QVector3D Generic6DofSpring2Constraint::angularUpperLimit() const
{
    return m_angularUpperLimit;
}

QVector3D Generic6DofSpring2Constraint::pivotA() const
{
    return m_pivotA;
}

QVector3D Generic6DofSpring2Constraint::pivotB() const
{
    return m_pivotB;
}

QQuaternion Generic6DofSpring2Constraint::rotationA() const
{
    return m_rotA;
}

QQuaternion Generic6DofSpring2Constraint::rotationB() const
{
    return m_rotB;
}

void Generic6DofSpring2Constraint::setLinearLowerLimit(QVector3D linearLowerLimit)
{
    if (m_linearLowerLimit == linearLowerLimit)
        return;

    m_linearLowerLimit = linearLowerLimit;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setLinearLowerLimit(q2b(linearLowerLimit));
    }
    emit linearLowerLimitChanged(m_linearLowerLimit);
}

void Generic6DofSpring2Constraint::setLinearUpperLimit(QVector3D linearUpLimit)
{
    if (m_linearUpperLimit == linearUpLimit)
        return;

    m_linearUpperLimit = linearUpLimit;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setLinearUpperLimit(q2b(linearUpLimit));
    }
    emit linearUpperLimitChanged(m_linearUpperLimit);
}

void Generic6DofSpring2Constraint::setAngularLowerLimit(QVector3D angularLowerLimit)
{
    if (m_angularLowerLimit == angularLowerLimit)
        return;

    m_angularLowerLimit = angularLowerLimit;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setAngularLowerLimit(q2b(radians(angularLowerLimit)));
    }
    emit angularLowerLimitChanged(m_angularLowerLimit);
}

void Generic6DofSpring2Constraint::setAngularUpperLimit(QVector3D angularUpLimit)
{
    if (m_angularUpperLimit == angularUpLimit)
        return;

    m_angularUpperLimit = angularUpLimit;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setAngularUpperLimit(q2b(radians(angularUpLimit)));
    }
    emit angularUpperLimitChanged(m_angularUpperLimit);
}

void Generic6DofSpring2Constraint::setPivotA(QVector3D pivotA)
{
    if (m_pivotA == pivotA)
        return;

    m_pivotA = pivotA;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit pivotAChanged(m_pivotA);
}

void Generic6DofSpring2Constraint::setPivotB(QVector3D pivotB)
{
    if (m_pivotB == pivotB)
        return;

    m_pivotB = pivotB;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit pivotBChanged(m_pivotB);
}

void Generic6DofSpring2Constraint::setRotationA(QQuaternion rotA)
{
    if(m_rotA==rotA)
        return;
    m_rotA = rotA;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit rotationAChanged(rotA);
}

void Generic6DofSpring2Constraint::setRotationB(QQuaternion rotB)
{
    if(m_rotB==rotB)
        return;
    m_rotB = rotB;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setFrames(q2b(transformA()), q2b(transformB()));
    }
    emit rotationAChanged(rotB);
}


qreal Generic6DofSpring2Constraint::yawA() const
{
    return m_rotA.toEulerAngles().y();
}

qreal Generic6DofSpring2Constraint::pitchA() const
{
    return m_rotA.toEulerAngles().x();
}

qreal Generic6DofSpring2Constraint::rollA() const
{
    return m_rotA.toEulerAngles().z();
}

qreal Generic6DofSpring2Constraint::yawB() const
{
    return m_rotB.toEulerAngles().y();
}

qreal Generic6DofSpring2Constraint::pitchB() const
{
    return m_rotB.toEulerAngles().x();
}

qreal Generic6DofSpring2Constraint::rollB() const
{
    return m_rotB.toEulerAngles().z();
}

void Generic6DofSpring2Constraint::setYawA(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yawA(), yaw))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitchA(), yaw, rollA()));
    emit yawAChanged(yaw);
}

void Generic6DofSpring2Constraint::setPitchA(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitchA(), pitch))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitch, yawA(), rollA()));
    emit pitchAChanged(pitch);
}

void Generic6DofSpring2Constraint::setRollA(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->rollA(), roll))
        return;

    setRotationA(QQuaternion::fromEulerAngles(pitchA(), yawA(), roll));
    emit rollAChanged(roll);
}

void Generic6DofSpring2Constraint::setYawB(qreal yaw)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->yawB(), yaw))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitchB(), yaw, rollB()));
    emit yawBChanged(yaw);
}

void Generic6DofSpring2Constraint::setPitchB(qreal pitch)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->pitchB(), pitch))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitch, yawB(), rollB()));
    emit pitchBChanged(pitch);
}

void Generic6DofSpring2Constraint::setRollB(qreal roll)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(this->rollB(), roll))
        return;

    setRotationB(QQuaternion::fromEulerAngles(pitchB(), yawB(), roll));
    emit rollBChanged(roll);
}

void Generic6DofSpring2Constraint::setLinearStiffness(QVector3D linearStiffness)
{
    if (m_linearStiffness == linearStiffness)
        return;

    m_linearStiffness = linearStiffness;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setStiffness(0, linearStiffness.x());
        generic6DofSpring2()->setStiffness(1, linearStiffness.y());
        generic6DofSpring2()->setStiffness(2, linearStiffness.z());
    }
    emit linearStiffnessChanged(m_linearStiffness);
}

void Generic6DofSpring2Constraint::setAngularStiffness(QVector3D angularStiffness)
{
    if (m_angularStiffness == angularStiffness)
        return;

    m_angularStiffness = angularStiffness;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setStiffness(3, angularStiffness.x());
        generic6DofSpring2()->setStiffness(4, angularStiffness.y());
        generic6DofSpring2()->setStiffness(5, angularStiffness.z());
    }
    emit angularStiffnessChanged(m_angularStiffness);
}

void Generic6DofSpring2Constraint::setLinearDamping(QVector3D linearDamping)
{
    if (m_linearDamping == linearDamping)
        return;

    m_linearDamping = linearDamping;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setDamping(0, linearDamping.x());
        generic6DofSpring2()->setDamping(1, linearDamping.y());
        generic6DofSpring2()->setDamping(2, linearDamping.z());
    }
    emit linearDampingChanged(m_linearDamping);
}

void Generic6DofSpring2Constraint::setAngularDamping(QVector3D angularDamping)
{
    if (m_angularDamping == angularDamping)
        return;

    m_angularDamping = angularDamping;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setDamping(3, angularDamping.x());
        generic6DofSpring2()->setDamping(4, angularDamping.y());
        generic6DofSpring2()->setDamping(5, angularDamping.z());
    }
    emit angularDampingChanged(m_angularDamping);
}

void Generic6DofSpring2Constraint::setLinearBounce(QVector3D linearBounce)
{
    if (m_linearBounce == linearBounce)
        return;

    m_linearBounce = linearBounce;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setBounce(0, m_linearBounce.x());
        generic6DofSpring2()->setBounce(1, m_linearBounce.y());
        generic6DofSpring2()->setBounce(2, m_linearBounce.z());
    }
    emit linearBounceChanged(m_linearBounce);
}

void Generic6DofSpring2Constraint::setAngularBounce(QVector3D angularBounce)
{
    if (m_angularBounce == angularBounce)
        return;

    m_angularBounce = angularBounce;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setBounce(3, m_angularBounce.x());
        generic6DofSpring2()->setBounce(4, m_angularBounce.y());
        generic6DofSpring2()->setBounce(5, m_angularBounce.z());
    }
    emit angularBounceChanged(m_angularBounce);
}

void Generic6DofSpring2Constraint::setLinearMotorXEnabled(bool linearMotorXEnabled)
{
    if (m_linearMotorXEnabled == linearMotorXEnabled)
        return;

    m_linearMotorXEnabled = linearMotorXEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableMotor(0, m_linearMotorXEnabled);
    }
    emit linearMotorXEnabledChanged(m_linearMotorXEnabled);
}

void Generic6DofSpring2Constraint::setLinearMotorYEnabled(bool linearMotorYEnabled)
{
    if (m_linearMotorYEnabled == linearMotorYEnabled)
        return;

    m_linearMotorYEnabled = linearMotorYEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableMotor(1, m_linearMotorYEnabled);
    }
    emit linearMotorYEnabledChanged(m_linearMotorYEnabled);
}

void Generic6DofSpring2Constraint::setLinearMotorZEnabled(bool linearMotorZEnabled)
{
    if (m_linearMotorZEnabled == linearMotorZEnabled)
        return;

    m_linearMotorZEnabled = linearMotorZEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableMotor(2, m_linearMotorZEnabled);
    }
    emit linearMotorZEnabledChanged(m_linearMotorZEnabled);
}

void Generic6DofSpring2Constraint::setAngularMotorXEnabled(bool angularMotorXEnabled)
{
    if (m_angularMotorXEnabled == angularMotorXEnabled)
        return;

    m_angularMotorXEnabled = angularMotorXEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableMotor(3, m_angularMotorXEnabled);
    }
    emit angularMotorXEnabledChanged(m_angularMotorXEnabled);
}

void Generic6DofSpring2Constraint::setAngularMotorYEnabled(bool angularMotorYEnabled)
{
    if (m_angularMotorYEnabled == angularMotorYEnabled)
        return;

    m_angularMotorYEnabled = angularMotorYEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableMotor(4, m_angularMotorYEnabled);
    }
    emit angularMotorYEnabledChanged(m_angularMotorYEnabled);
}

void Generic6DofSpring2Constraint::setAngularMotorZEnabled(bool angularMotorZEnabled)
{
    if (m_angularMotorZEnabled == angularMotorZEnabled)
        return;

    m_angularMotorZEnabled = angularMotorZEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->enableMotor(5, m_angularMotorZEnabled);
    }
    emit angularMotorZEnabledChanged(m_angularMotorZEnabled);
}

void Generic6DofSpring2Constraint::setLinearServoXEnabled(bool linearServoXEnabled)
{
    if (m_linearServoXEnabled == linearServoXEnabled)
        return;

    m_linearServoXEnabled = linearServoXEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServo(0, m_linearServoXEnabled);
    }
    emit linearServoXEnabledChanged(m_linearServoXEnabled);
}

void Generic6DofSpring2Constraint::setLinearServoYEnabled(bool linearServoYEnabled)
{
    if (m_linearServoYEnabled == linearServoYEnabled)
        return;

    m_linearServoYEnabled = linearServoYEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServo(1, m_linearServoYEnabled);
    }
    emit linearServoYEnabledChanged(m_linearServoYEnabled);
}

void Generic6DofSpring2Constraint::setLinearServoZEnabled(bool linearServoZEnabled)
{
    if (m_linearServoZEnabled == linearServoZEnabled)
        return;

    m_linearServoZEnabled = linearServoZEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServo(2, m_linearServoZEnabled);
    }
    emit linearServoZEnabledChanged(m_linearServoZEnabled);
}

void Generic6DofSpring2Constraint::setAngularServoXEnabled(bool angularServoXEnabled)
{
    if (m_angularServoXEnabled == angularServoXEnabled)
        return;

    m_angularServoXEnabled = angularServoXEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServo(3, m_angularServoXEnabled);
    }
    emit angularServoXEnabledChanged(m_angularServoXEnabled);
}

void Generic6DofSpring2Constraint::setAngularServoYEnabled(bool angularServoYEnabled)
{
    if (m_angularServoYEnabled == angularServoYEnabled)
        return;

    m_angularServoYEnabled = angularServoYEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServo(4, m_angularServoYEnabled);
    }
    emit angularServoYEnabledChanged(m_angularServoYEnabled);
}

void Generic6DofSpring2Constraint::setAngularServoZEnabled(bool angularServoZEnabled)
{
    if (m_angularServoZEnabled == angularServoZEnabled)
        return;

    m_angularServoZEnabled = angularServoZEnabled;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServo(5, m_angularServoZEnabled);
    }
    emit angularServoZEnabledChanged(m_angularServoZEnabled);
}

void Generic6DofSpring2Constraint::setLinearTargetVelocity(QVector3D linearTargetVelocity)
{
    if (m_linearTargetVelocity == linearTargetVelocity)
        return;

    m_linearTargetVelocity = linearTargetVelocity;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setTargetVelocity(0, m_linearTargetVelocity.x());
        generic6DofSpring2()->setTargetVelocity(1, m_linearTargetVelocity.y());
        generic6DofSpring2()->setTargetVelocity(2, m_linearTargetVelocity.z());
    }
    emit linearTargetVelocityChanged(m_linearTargetVelocity);
}

void Generic6DofSpring2Constraint::setAngularTargetVelocity(QVector3D angularTargetVelocity)
{
    if (m_angularTargetVelocity == angularTargetVelocity)
        return;

    m_angularTargetVelocity = angularTargetVelocity;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setTargetVelocity(3, m_angularTargetVelocity.x());
        generic6DofSpring2()->setTargetVelocity(4, m_angularTargetVelocity.y());
        generic6DofSpring2()->setTargetVelocity(5, m_angularTargetVelocity.z());
    }
    emit angularTargetVelocityChanged(m_angularTargetVelocity);
}

void Generic6DofSpring2Constraint::setLinearServoTarget(QVector3D linearServoTarget)
{
    if (m_linearServoTarget == linearServoTarget)
        return;

    m_linearServoTarget = linearServoTarget;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServoTarget(0, m_linearServoTarget.x());
        generic6DofSpring2()->setServoTarget(1, m_linearServoTarget.y());
        generic6DofSpring2()->setServoTarget(2, m_linearServoTarget.z());
    }
    emit linearServoTargetChanged(m_linearServoTarget);
}

void Generic6DofSpring2Constraint::setAngularServoTarget(QVector3D angularServoTarget)
{
    if (m_angularServoTarget == angularServoTarget)
        return;

    m_angularServoTarget = angularServoTarget;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setServoTarget(3, qDegreesToRadians(m_angularServoTarget.x()));
        generic6DofSpring2()->setServoTarget(4, qDegreesToRadians(m_angularServoTarget.y()));
        generic6DofSpring2()->setServoTarget(5, qDegreesToRadians(m_angularServoTarget.z()));
    }
    emit angularServoTargetChanged(m_angularServoTarget);
}

void Generic6DofSpring2Constraint::setMaxLinearMotorForce(QVector3D maxLinearMotorForce)
{
    if (m_maxLinearMotorForce == maxLinearMotorForce)
        return;

    m_maxLinearMotorForce = maxLinearMotorForce;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setMaxMotorForce(0, m_maxLinearMotorForce.x());
        generic6DofSpring2()->setMaxMotorForce(1, m_maxLinearMotorForce.y());
        generic6DofSpring2()->setMaxMotorForce(2, m_maxLinearMotorForce.z());
    }
    emit maxLinearMotorForceChanged(m_maxLinearMotorForce);
}

void Generic6DofSpring2Constraint::setMaxAngularMotorForce(QVector3D maxAngularMotorForce)
{
    if (m_maxAngularMotorForce == maxAngularMotorForce)
        return;

    m_maxAngularMotorForce = maxAngularMotorForce;
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setMaxMotorForce(3, m_maxAngularMotorForce.x());
        generic6DofSpring2()->setMaxMotorForce(4, m_maxAngularMotorForce.y());
        generic6DofSpring2()->setMaxMotorForce(5, m_maxAngularMotorForce.z());
    }
    emit maxAngularMotorForceChanged(m_maxAngularMotorForce);
}

QMatrix4x4 Generic6DofSpring2Constraint::transformA() const
{
    QMatrix4x4 mat;
    mat.translate(m_pivotA);
    mat.rotate(m_rotA);
    return mat;
}

QMatrix4x4 Generic6DofSpring2Constraint::transformB() const
{
    QMatrix4x4 mat;
    mat.translate(m_pivotB);
    mat.rotate(m_rotB);
    return mat;
}

void Generic6DofSpring2Constraint::postCreate()
{
    Constraint::postCreate();
    if(generic6DofSpring2()) {
        generic6DofSpring2()->setLinearLowerLimit(q2b(m_linearLowerLimit));
        generic6DofSpring2()->setLinearUpperLimit(q2b(m_linearUpperLimit));
        generic6DofSpring2()->setAngularLowerLimit(q2b(radians(m_angularLowerLimit)));
        generic6DofSpring2()->setAngularUpperLimit(q2b(radians(m_angularUpperLimit)));

        generic6DofSpring2()->enableSpring(0, m_linearSpringXEnabled);
        generic6DofSpring2()->enableSpring(1, m_linearSpringYEnabled);
        generic6DofSpring2()->enableSpring(2, m_linearSpringZEnabled);
        generic6DofSpring2()->enableSpring(3, m_angularSpringXEnabled);
        generic6DofSpring2()->enableSpring(4, m_angularSpringYEnabled);
        generic6DofSpring2()->enableSpring(5, m_angularSpringZEnabled);

        generic6DofSpring2()->setStiffness(0, m_linearStiffness.x());
        generic6DofSpring2()->setStiffness(1, m_linearStiffness.y());
        generic6DofSpring2()->setStiffness(2, m_linearStiffness.z());
        generic6DofSpring2()->setStiffness(3, m_angularStiffness.x());
        generic6DofSpring2()->setStiffness(4, m_angularStiffness.y());
        generic6DofSpring2()->setStiffness(5, m_angularStiffness.z());

        generic6DofSpring2()->setDamping(0, m_linearDamping.x());
        generic6DofSpring2()->setDamping(1, m_linearDamping.y());
        generic6DofSpring2()->setDamping(2, m_linearDamping.z());
        generic6DofSpring2()->setDamping(3, m_angularDamping.x());
        generic6DofSpring2()->setDamping(4, m_angularDamping.y());
        generic6DofSpring2()->setDamping(5, m_angularDamping.z());

        generic6DofSpring2()->setBounce(0, m_linearBounce.x());
        generic6DofSpring2()->setBounce(1, m_linearBounce.y());
        generic6DofSpring2()->setBounce(2, m_linearBounce.z());
        generic6DofSpring2()->setBounce(3, m_angularBounce.x());
        generic6DofSpring2()->setBounce(4, m_angularBounce.y());
        generic6DofSpring2()->setBounce(5, m_angularBounce.z());

        generic6DofSpring2()->enableMotor(0, m_linearMotorXEnabled);
        generic6DofSpring2()->enableMotor(1, m_linearMotorYEnabled);
        generic6DofSpring2()->enableMotor(2, m_linearMotorZEnabled);
        generic6DofSpring2()->enableMotor(3, m_angularMotorXEnabled);
        generic6DofSpring2()->enableMotor(4, m_angularMotorYEnabled);
        generic6DofSpring2()->enableMotor(5, m_angularMotorZEnabled);

        generic6DofSpring2()->setServo(0, m_linearServoXEnabled);
        generic6DofSpring2()->setServo(1, m_linearServoYEnabled);
        generic6DofSpring2()->setServo(2, m_linearServoZEnabled);
        generic6DofSpring2()->setServo(3, m_angularServoXEnabled);
        generic6DofSpring2()->setServo(4, m_angularServoYEnabled);
        generic6DofSpring2()->setServo(5, m_angularServoZEnabled);

        generic6DofSpring2()->setTargetVelocity(0, m_linearTargetVelocity.x());
        generic6DofSpring2()->setTargetVelocity(1, m_linearTargetVelocity.y());
        generic6DofSpring2()->setTargetVelocity(2, m_linearTargetVelocity.z());
        generic6DofSpring2()->setTargetVelocity(3, m_angularTargetVelocity.x());
        generic6DofSpring2()->setTargetVelocity(4, m_angularTargetVelocity.y());
        generic6DofSpring2()->setTargetVelocity(5, m_angularTargetVelocity.z());

        generic6DofSpring2()->setServoTarget(0, m_linearServoTarget.x());
        generic6DofSpring2()->setServoTarget(1, m_linearServoTarget.y());
        generic6DofSpring2()->setServoTarget(2, m_linearServoTarget.z());
        generic6DofSpring2()->setServoTarget(3, qDegreesToRadians(m_angularServoTarget.x()));
        generic6DofSpring2()->setServoTarget(4, qDegreesToRadians(m_angularServoTarget.y()));
        generic6DofSpring2()->setServoTarget(5, qDegreesToRadians(m_angularServoTarget.z()));

        generic6DofSpring2()->setMaxMotorForce(0, m_maxLinearMotorForce.x());
        generic6DofSpring2()->setMaxMotorForce(1, m_maxLinearMotorForce.y());
        generic6DofSpring2()->setMaxMotorForce(2, m_maxLinearMotorForce.z());
        generic6DofSpring2()->setMaxMotorForce(3, m_maxAngularMotorForce.x());
        generic6DofSpring2()->setMaxMotorForce(4, m_maxAngularMotorForce.y());
        generic6DofSpring2()->setMaxMotorForce(5, m_maxAngularMotorForce.z());
    }
}

}
