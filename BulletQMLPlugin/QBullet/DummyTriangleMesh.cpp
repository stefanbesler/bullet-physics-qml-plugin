/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "btIndexedMeshHelper.h"
#include "DummyTriangleMesh.h"

namespace QBullet {

DummyTriangleMesh::DummyTriangleMesh(QObject *parent)
    : TriangleMesh(parent)
{

}

void DummyTriangleMesh::onComponentComplete()
{
    TriangleMesh::onComponentComplete();
    QSharedPointer<btIndexedMeshHelper> mesh(new btIndexedMeshHelper(4, 2));
    mesh->setTriangle(0, 0, 2, 1);
    mesh->setTriangle(1, 1, 2, 3);

    mesh->setVertex(0, 0, 20, 0);
    mesh->setVertex(1, 50, 0, 0);
    mesh->setVertex(2, 0, -10, 50);
    mesh->setVertex(3, 50, 0, 50);

    mesh->updateVertexNormal();
    setMesh(mesh);

}

}

#include "moc_DummyTriangleMesh.cpp"
