/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "Point2PointConstraint.h"
#include "QBullet.h"

namespace QBullet
{

Point2PointConstraint::Point2PointConstraint(QObject *parent)
    : Constraint(parent)
    , m_impulseClamp(0)
    , m_tau(0.3)
    , m_damping(1.0)
{

}

QVector3D Point2PointConstraint::pivotA() const
{
    return m_pivotA;
}

QVector3D Point2PointConstraint::pivotB() const
{
    return m_pivotB;
}

void Point2PointConstraint::setPivotA(QVector3D pivotInA)
{
    if (m_pivotA == pivotInA)
        return;

    m_pivotA = pivotInA;
    if(p2p()) {
        p2p()->setPivotA(q2b(pivotInA));
    }
    emit pivotAChanged(m_pivotA);
}

void Point2PointConstraint::setPivotB(QVector3D pivotInB)
{
    if (m_pivotB == pivotInB)
        return;

    m_pivotB = pivotInB;
    if(p2p()) {
        p2p()->setPivotB(q2b(pivotInB));
    }
    emit pivotBChanged(m_pivotB);
}

void Point2PointConstraint::setImpulseClamp(qreal impulseClamp)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_impulseClamp, impulseClamp))
        return;

    m_impulseClamp = impulseClamp;
    if(p2p()) {
        p2p()->m_setting.m_impulseClamp = impulseClamp;
    }
    emit impulseClampChanged(m_impulseClamp);
}

void Point2PointConstraint::setTau(qreal tau)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_tau, tau))
        return;

    m_tau = tau;
    if(p2p()) {
        p2p()->m_setting.m_tau = m_tau;
    }
    emit tauChanged(m_tau);
}

void Point2PointConstraint::setDamping(qreal damping)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_damping, damping))
        return;

    m_damping = damping;
    if(p2p()) {
        p2p()->m_setting.m_damping = m_damping;
    }
    emit dampingChanged(m_damping);
}

void Point2PointConstraint::postCreate()
{
    Constraint::postCreate();
    if(p2p()) {
        p2p()->m_setting.m_impulseClamp = m_impulseClamp;
        p2p()->m_setting.m_tau = m_tau;
        p2p()->m_setting.m_damping = m_damping;
    }
}

QSharedPointer<btTypedConstraint> Point2PointConstraint::create() const
{
    QSharedPointer<btTypedConstraint> p2p;
    if(rbA()) {
        if(rbB()) {
            p2p.reset(new btPoint2PointConstraint(*rbA().data(), *rbB().data(), q2b(m_pivotA), q2b(m_pivotB)));
        } else {
            p2p.reset(new btPoint2PointConstraint(*rbA().data(), q2b(m_pivotA)));
        }
    }
    return p2p;
}

QSharedPointer<btPoint2PointConstraint> Point2PointConstraint::p2p() const
{
    return constraint().dynamicCast<btPoint2PointConstraint>();
}

qreal Point2PointConstraint::impulseClamp() const
{
    return m_impulseClamp;
}

qreal Point2PointConstraint::tau() const
{
    return m_tau;
}

qreal Point2PointConstraint::damping() const
{
    return m_damping;
}

}
