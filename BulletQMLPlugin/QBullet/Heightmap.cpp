/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <QtDebug>
#include <QtMath>
#include <QImage>
#include <btBulletDynamicsCommon.h>
#include "btIndexedMeshHelper.h"
#include "Heightmap.h"

namespace QBullet {

Heightmap::Heightmap(QObject *parent)
    : TriangleMesh(parent)
    , m_scale(1., 1., 1.)
    , m_autoUpdateMeshVertex(false)
    , m_diamondSubdivision(true)
{

}

QUrl Heightmap::heightmap() const
{
    return m_url;
}

QVector3D Heightmap::scale() const
{
    return m_scale;
}

void Heightmap::testDynamicChange()
{
    QSharedPointer<btIndexedMeshHelper> mesh = this->mesh();
    if(mesh) {
        QRect rect(m_heightmapSize.width()/2, m_heightmapSize.height()/2,
                   50, 50);
        //Update vertices
        for(int row=rect.top(); row<rect.bottom(); row++) {
            for(int column=rect.left(); column<rect.right(); column++) {
                int index = row*m_heightmapSize.width()+column;
                qreal height = 20;
                m_heightmapData->setHeightValue(index, height/m_scale.y());
                QVector3D v = mesh->vertex(index);
                v.setY(height);
                mesh->setVertex(index, v);
            }
        }
        //Update normals
        for(int row=rect.top(); row<rect.bottom(); row++) {
            for(int column=rect.left(); column<rect.right(); column++) {

            }
        }
        mesh->updateVertexNormal();
        mesh->increaseVertexVersion();
        setChanged();
    }
}

void Heightmap::substract(const QVector3D &center, qreal radius)
{
    QSharedPointer<btIndexedMeshHelper> mesh = this->mesh();
    if(mesh.isNull())
        return;
    if(m_heightmapData.isNull()||!m_heightmapData->isValid())
        return;

    QVector3D rawCenter = (QVector3D(width(), 0, height())/2) + (center/m_scale);
    //qDebug()<<rawCenter;
    qreal diameter = radius *2;
    QRect rect(QPoint(), QSize(0.5 + diameter/m_scale.x(), 0.5 + diameter/m_scale.z()));
    rect.moveCenter(QPoint(rawCenter.x(), rawCenter.z()));

    rect = rect.intersected(m_heightmapData->rect());
    //Update vertices
    for(int row=rect.top(); row<=rect.bottom(); row++) {
        for(int column=rect.left(); column<=rect.right(); column++) {
            int index = this->index(row, column);
            QVector3D vertex = this->vertex(index);
            QVector3D v = vertex - center;
            v.setY(0);//only use the horizontal vector.
            if(v.length()>radius)
                continue;

            qreal height = center.y() - qSqrt(radius*radius - v.length()*v.length());
            if(vertex.y()<height)
                continue;
            //qDebug()<<height;
            vertex.setY(height);
            setVertex(index, vertex);
        }
    }
    //Update normals
    for(int row=rect.top(); row<rect.bottom(); row++) {
        for(int column=rect.left(); column<rect.right(); column++) {
            updateNormal(row, column);
        }
    }
    //mesh->updateVertexNormal();//Replaced with above faster code.

    setChanged();
}

QRect Heightmap::toHeightmapRectXZ(const QVector3D &min, const QVector3D &max) const
{
    if(m_scale.x()<=0||
            m_scale.z()<=0)
        return QRect();
    QVector3D sv = max - min;
    QRect r = QRect(min.x()/m_scale.x(), min.z()/m_scale.z(), 0.5 + sv.x()/m_scale.x(), 0.5 + sv.z()/m_scale.z());
    r.moveCenter(r.center() + (QPointF(width(), height())/2 + QPointF(0.5, 0.5)).toPoint());
    r = r.intersected(m_heightmapData->rect());
    return r;
}

QVector3D Heightmap::vertex(int index) const
{
    QSharedPointer<btIndexedMeshHelper> mesh = this->mesh();
    if(mesh.isNull())
        return QVector3D();
    return mesh->vertex(index);
}

QVector3D Heightmap::vertex(int row, int column) const
{
    return vertex(index(row, column));
}

void Heightmap::setVertex(int index, const QVector3D &v)
{
    QSharedPointer<btIndexedMeshHelper> mesh = this->mesh();
    if(mesh.isNull())
        return;
    if(m_heightmapData.isNull())
        return;
    if(m_scale.y()<=0)
        return;
    m_heightmapData->setHeightValue(index, v.y()/m_scale.y());
    QVector3D t(v);
    t.setY(m_heightmapData->heightValue(index)*m_scale.y());
    mesh->setVertex(index, v);
}

void Heightmap::setVertex(int row, int column, const QVector3D &v)
{
    setVertex(index(row, column), v);
}

void Heightmap::updateNormal(int row, int column)
{
    int index = this->index(row, column);
    QVector3D normal;
    int cnt(0);
    if(row>0) {
        if(column>0) {
            // top-left
            normal += QVector3D::normal(vertex(index - 1), vertex(index), vertex(index - m_heightmapData->width()));
            cnt++;
        }
        if(column<m_heightmapData->width() - 1) {
            // top-right
            normal += QVector3D::normal(vertex(index), vertex(index + 1), vertex(index - m_heightmapData->width()));
            cnt++;
        }
    }
    if(row<m_heightmapData->height() - 1) {
        if(column>0) {
            // bottom-left
            normal += QVector3D::normal(vertex(index - 1), vertex(index + m_heightmapData->width()), vertex(index));
            cnt++;
        }
        if(column<m_heightmapData->width() - 1) {
            // bottom-right
            normal += QVector3D::normal(vertex(index), vertex(index + m_heightmapData->width()), vertex(index + 1));
            cnt++;
        }
    }
    if(cnt>0) {
        setNormal(index, normal/cnt);
    }
}

QVector3D Heightmap::normal(int index) const
{
    QSharedPointer<btIndexedMeshHelper> mesh = this->mesh();
    if(mesh.isNull())
        return QVector3D();
    return mesh->normal(index);
}

QVector3D Heightmap::normal(int row, int column) const
{
    return normal(index(row, column));
}

void Heightmap::setNormal(int index, const QVector3D &n)
{
    QSharedPointer<btIndexedMeshHelper> mesh = this->mesh();
    if(mesh.isNull())
        return;
    mesh->setNormal(index, n);
}

void Heightmap::setNormal(int row, int column, const QVector3D &n)
{
    setNormal(index(row, column), n);
}

int Heightmap::index(int row, int column) const
{
    Q_ASSERT(row>=0&&row<height()&&
             column>=0&&column<width());
    return row*width() + column;
}

bool Heightmap::find(QVector3D pos, int &row, int &column)
{
    if(m_scale.z()<=0||
            m_scale.x()<=0)
        return false;
    row = (0.5 + pos.z()/m_scale.z()) + height()/2;
    column = (0.5 + pos.x()/m_scale.x()) + width()/2;

    return row>=0&&row<height()&&column>=0&&column<width();
}

bool Heightmap::autoUpdateMeshVertex() const
{
    return m_autoUpdateMeshVertex;
}

QSharedPointer<HeightmapData> Heightmap::heightmapData() const
{
    return m_heightmapData;
}

bool Heightmap::diamondSubdivision() const
{
    return m_diamondSubdivision;
}

qreal Heightmap::minHeightValue() const
{
    return m_heightmapData?m_heightmapData->minHeightValue()*m_scale.y():0;
}

qreal Heightmap::maxHeightValue() const
{
    return m_heightmapData?m_heightmapData->maxHeightValue()*m_scale.y():0;
}

qreal Heightmap::halfHeightValue() const
{
    return (minHeightValue() + maxHeightValue())/2;
}

int Heightmap::height() const
{
    return m_heightmapData?m_heightmapData->height():0;
}

int Heightmap::width() const
{
    return m_heightmapData?m_heightmapData->width():0;
}

QSize Heightmap::size() const
{
    return m_heightmapData?m_heightmapData->size(): QSize();
}

void Heightmap::setHeightmap(QUrl heightmap)
{
    if (m_url == heightmap)
        return;
    m_url = heightmap;
    m_heightmapData.reset(new HeightmapData(heightmap));
    resetMesh();
    emit heightmapChanged(m_url);
}

void Heightmap::setScale(QVector3D scale)
{
    if(scale.x()<=0||scale.y()<=0||scale.z()<=0)
        return;
    if (m_scale == scale)
        return;

    m_scale = scale;
    resetMesh();
    emit scaleChanged(m_scale);
}

void Heightmap::setAutoUpdateMeshVertex(bool autoUpdateMeshVertex)
{
    if (m_autoUpdateMeshVertex == autoUpdateMeshVertex)
        return;

    m_autoUpdateMeshVertex = autoUpdateMeshVertex;
    emit autoUpdateMeshVertexChanged(m_autoUpdateMeshVertex);
}

void Heightmap::setDiamondSubdivision(bool diamondSubdivision)
{
    if (m_diamondSubdivision == diamondSubdivision)
        return;

    m_diamondSubdivision = diamondSubdivision;
    resetMesh();
    emit diamondSubdivisionChanged(m_diamondSubdivision);
}

bool Heightmap::isATriangle(int a, int b, int c) const
{
    if(m_heightmapData.isNull())
        return false;
    return m_heightmapData->heightValue(a)>=0||
            m_heightmapData->heightValue(b)>=0||
            m_heightmapData->heightValue(c)>=0;
}

void Heightmap::resetMesh()
{
    if(!isComplete())
        return;
    QSize oldSize = size();
    m_heightmapData.reset(new HeightmapData(m_url));
    if(!m_heightmapData->isValid()) {
        m_heightmapData.clear();
        emit sizeChanged(size());
        return;
    }
    qDebug()<<m_url.toString();
    QImage image(m_url.toString().replace("qrc:", ":"));
    if(!image.isNull()) {
        m_heightmapSize = m_heightmapData->size();
        qreal halfWidth = m_heightmapData->width()/2.0;
        qreal halfHeight = m_heightmapData->height()/2.0;
        int vertexCount = m_heightmapData->count();
        int rows = m_heightmapData->height() - 1;
        int columns = m_heightmapData->width() - 1;
        int maxTriangleCount = rows * columns * 2;

        QSharedPointer<btIndexedMeshHelper> mesh(new btIndexedMeshHelper(vertexCount, maxTriangleCount));
        for(int i=0; i<vertexCount; i++) {
            int row = (i/m_heightmapData->width());
            int column = (i%m_heightmapData->width());
            qreal height = m_heightmapData->heightValue(row, column);
            if(height<0)
                height = m_heightmapData->minHeightValue();
            QVector3D v = QVector3D((column-halfWidth), height, (row-halfHeight));
            mesh->setVertex(i, v*m_scale);
            mesh->setTexCoord(i, QPointF(column/(qreal)m_heightmapData->width(), row/(qreal)m_heightmapData->height()));
        }

        int triangle(0);
        int vertexIndex(0);
        for(int row =0; row<rows; row++) {
            for (int column = 0; column< columns; column++) {
                if(((row%2)==0||(column%2)==0)&&m_diamondSubdivision) {
                    if(isATriangle(vertexIndex, vertexIndex + image.width(), vertexIndex +1)) {
                        mesh->setTriangle(triangle++, vertexIndex, vertexIndex + image.width(), vertexIndex +1);
                    }
                    if(isATriangle(vertexIndex+1, vertexIndex + image.width(), vertexIndex + image.width() + 1)) {
                        mesh->setTriangle(triangle++, vertexIndex+1, vertexIndex + image.width(), vertexIndex + image.width() + 1);
                    }
                } else {
                    if(isATriangle(vertexIndex, vertexIndex + image.width(), vertexIndex + image.width() + 1)) {
                        mesh->setTriangle(triangle++, vertexIndex, vertexIndex + image.width(), vertexIndex + image.width() + 1);
                    }
                    if(isATriangle(vertexIndex, vertexIndex + image.width() + 1, vertexIndex + 1)) {
                        mesh->setTriangle(triangle++, vertexIndex, vertexIndex + image.width() + 1, vertexIndex + 1);
                    }
                }
                vertexIndex++;
            }
            vertexIndex++;
        }

        mesh->m_numTriangles = triangle;

        mesh->updateVertexNormal();
        setMesh(mesh);
        emit minHeightValueChanged(minHeightValue());
        emit maxHeightValueChanged(maxHeightValue());
        emit halfHeightValueChanged(halfHeightValue());
        if(oldSize!=size())
            emit sizeChanged(size());
    }
}

void Heightmap::onComponentComplete()
{
    TriangleMesh::onComponentComplete();
    resetMesh();
}

HeightmapData::HeightmapData(QUrl url, qreal heightValueScale)
    : m_url(url)
    , m_data(0)
    , m_heightValueScale(heightValueScale)
    , m_minHeightValue(0.0)
    , m_maxHeightValue(-1.0)
    , m_count(0)
{
    Q_ASSERT(m_heightValueScale>0);
    QImage image(url.toString().replace("qrc:", ":"));
    if(!image.isNull()) {
        m_size = image.size();
        m_count = m_size.width()*m_size.height();
        m_data = new float[m_count];
        for(int i=0; i<m_count; i++) {
            int y = (i/image.width());
            int x = (i%image.width());
            QRgb color = image.pixel(x, y);
            qreal heightValue = m_heightValueScale * (qRed(color) + qGreen(color) + qBlue(color))/3.0;
            if(qAlpha(color)==0)
                heightValue = -1;
            m_data[i] = heightValue;
            if(heightValue>=0) {
                if(m_maxHeightValue<m_minHeightValue) {
                    m_minHeightValue = m_maxHeightValue = heightValue;
                } else {
                    if(m_minHeightValue>heightValue)
                        m_minHeightValue = heightValue;
                    if(m_maxHeightValue<heightValue)
                        m_maxHeightValue = heightValue;
                }
            }
        }
    }
    m_minHeightValue = 0;
    m_maxHeightValue = 256*heightValueScale;
}

HeightmapData::~HeightmapData()
{
    if(m_data)
        delete []m_data;
}

bool HeightmapData::isValid() const
{
    return m_data!=0;
}

QRect HeightmapData::rect() const
{
    return QRect(QPoint(), size());
}

QUrl HeightmapData::url() const
{
    return m_url;
}

QSize HeightmapData::size() const
{
    return m_size;
}

int HeightmapData::width() const
{
    return m_size.width();
}

int HeightmapData::height() const
{
    return m_size.height();
}

const float *HeightmapData::data() const
{
    return m_data;
}

qreal HeightmapData::heightValueScale() const
{
    return m_heightValueScale;
}

qreal HeightmapData::minHeightValue() const
{
    return m_minHeightValue;
}

qreal HeightmapData::maxHeightValue() const
{
    return m_maxHeightValue;
}

qreal HeightmapData::heightValue(int index) const
{
    Q_ASSERT(index>=0&&index<m_count);
    return m_data[index];
}

qreal HeightmapData::heightValue(int row, int column) const
{
    Q_ASSERT(row>=0&&row<m_size.height()&&
             column>=0&&column<m_size.width());

    int addr = row * m_size.width() + column;
    return m_data[addr];
}

void HeightmapData::setHeightValue(int index, qreal value)
{
    Q_ASSERT(index>=0&&index<m_count);
    if(value<m_minHeightValue) {
        value = m_minHeightValue;
    } else if(value>m_maxHeightValue) {
        value = m_maxHeightValue;
    }
    m_data[index] = value;
}

void HeightmapData::setHeightValue(int row, int column, qreal value)
{
    Q_ASSERT(row>=0&&row<m_size.height()&&
             column>=0&&column<m_size.width());

    int addr = row * m_size.width() + column;
    m_data[addr] = value;
}

int HeightmapData::count() const
{
    return m_count;
}

}

#include "moc_Heightmap.cpp"

