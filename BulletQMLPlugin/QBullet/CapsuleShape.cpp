/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>
#include "CapsuleShape.h"

namespace QBullet {

CapsuleShape::CapsuleShape(QObject *parent)
    : ConvexShape(parent)
    , m_radius(0.5)
    , m_height(2.0)
    , m_upAxis(Axis_Y)
{

}

QSharedPointer<btCapsuleShape> CapsuleShape::capsuleShape() const
{
    return shape().dynamicCast<btCapsuleShape>();
}

qreal CapsuleShape::radius() const
{
    return m_radius;
}

qreal CapsuleShape::height() const
{
    return m_height;
}


qreal CapsuleShape::totalHeight() const
{
    return m_height + m_radius*2;
}

CapsuleShape::Axis CapsuleShape::upAxis() const
{
    return m_upAxis;
}

void CapsuleShape::setRadius(qreal radius)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_radius, radius))
        return;

    m_radius = radius;
    resetShape();
    emit radiusChanged(m_radius);
}

void CapsuleShape::setHeight(qreal height)
{
    //qWarning("Floating point comparison needs context sanity check");
    if (qFuzzyCompare(m_height, height))
        return;

    m_height = height;
    resetShape();
    emit heightChanged(m_height);
    emit totalHeightChanged(totalHeight());
}

void CapsuleShape::setTotalHeight(qreal totalHeight)
{
    setHeight(totalHeight - m_radius*2);
}

void CapsuleShape::setUpAxis(CapsuleShape::Axis axis)
{
    if(m_upAxis==axis)
        return;
    m_upAxis = axis;
    resetShape();
    emit upAxisChanged(axis);
}

QSharedPointer<btCollisionShape> CapsuleShape::create() const
{
    switch(m_upAxis) {
    case Axis_X:
        return QSharedPointer<btCollisionShape>(new btCapsuleShapeX(m_radius, m_height));
    case Axis_Z:
        return QSharedPointer<btCollisionShape>(new btCapsuleShapeZ(m_radius, m_height));

    }
    return QSharedPointer<btCollisionShape>(new btCapsuleShape(m_radius, m_height));
}

}
