/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>

#include "SphereShape.h"

namespace QBullet {

SphereShape::SphereShape(QObject *parent)
    : ConvexShape(parent)
    , m_radius(1)
{
}

SphereShape::~SphereShape()
{
}

qreal SphereShape::radius() const
{
    return m_radius;
}

void SphereShape::setRadius(qreal radius)
{
    if (qFuzzyCompare(m_radius, radius))
        return;
    m_radius = radius;
    resetShape();
    emit radiusChanged(m_radius);
}

QSharedPointer<btCollisionShape> SphereShape::create() const
{
    return QSharedPointer<btCollisionShape>(new btSphereShape(m_radius));
}

QSharedPointer<btSphereShape> SphereShape::sphere() const
{
    return shape().dynamicCast<btSphereShape>();
}

}

#include "moc_SphereShape.cpp"
