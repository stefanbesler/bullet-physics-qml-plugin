/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef QBULLET_H
#define QBULLET_H

#include <qmath.h>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector3D>

#include <btBulletDynamicsCommon.h>

namespace QBullet {

inline QVector3D radians(const QVector3D &vec3)
{
    return QVector3D(qDegreesToRadians(vec3.x()), qDegreesToRadians(vec3.y()), qDegreesToRadians(vec3.z()));
}

inline QVector3D degrees(const QVector3D &vec3)
{
    return QVector3D(qRadiansToDegrees(vec3.x()), qRadiansToDegrees(vec3.y()), qRadiansToDegrees(vec3.z()));
}

inline QVector3D b2q(const btVector3 &vec3)
{
    return QVector3D(vec3.x(), vec3.y(), vec3.z());
}

inline QQuaternion b2q(const btQuaternion &rotation)
{
    return QQuaternion(rotation.getW(), rotation.getX(), rotation.getY(), rotation.getZ());
}

inline QMatrix4x4 b2q(const btTransform &trans)
{
    btScalar m[16];
    trans.getOpenGLMatrix(m);
#ifdef BT_USE_DOUBLE_PRECISION
    /*
    Fix me: if btScalar is double, need to convert to a float array before
    being sent to QMatrix4x4.
    */
    Q_ASSERT(false);
#endif
    QMatrix4x4 qm;
    memcpy(qm.data(), m, sizeof(float)*16);
    return qm;
}

inline btVector3 q2b(const QVector3D &vec3)
{
    return btVector3(vec3.x(), vec3.y(), vec3.z());
}

inline btQuaternion q2b(const QQuaternion &rotation)
{
    return btQuaternion(rotation.x(), rotation.y(), rotation.z(), rotation.scalar());
}

inline btTransform q2b(const QMatrix4x4 &mat)
{
    btTransform trans;
    btScalar m[16];
#ifdef BT_USE_DOUBLE_PRECISION
    /*
    Fix me: if btScalar is double, need to convert to a float array before
    being sent to QMatrix4x4.
    */
    Q_ASSERT(false);
#endif
    memcpy(m, mat.data(), sizeof(float)*16);
    trans.setIdentity();
    trans.setFromOpenGLMatrix(m);
    return trans;
}

}

#endif // QBULLET_H
