/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#ifndef GENERIC6DOFSPRING2CONSTRAINT_H
#define GENERIC6DOFSPRING2CONSTRAINT_H

#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector3D>
#include "Constraint.h"

class btGeneric6DofSpring2Constraint;

namespace QBullet
{

/*!
\class Generic6DofSpring2Constraint Generic6DofSpring2Constraint.h <QBullet/Generic6DofSpring2Constraint.h>

This generic constraint can emulate a variety of standard constraints, by
configuring each of the 6 degrees of freedom (DoF). The first 3 DoF axis are
linear axis, which represent translation of rigid bodies, and the latter 3 axis
represent the angular motion. Each axis can be either locked, free or limited.
Note that several combinations that include free and/or limited angular degrees
of freedom are undefined.

Configuring limits

On construction of a new Generic6DofSpring2Constraint, all axis are locked. The
limits of the 6DoF constraint are set through 4 vectors. Two represent the
upper and lower limits of the angular motion and the other two do the same for
the linear motion.

For each axis, if

lower limit = upper limit
The axis is locked.

lower limit < upper limit
The axis is limited between the specified values.

lower limit > upper limit
The axis is free and has no limits.

\see http://bulletphysics.org/mediawiki-1.5.8/index.php/Constraints
*/
class Generic6DofSpring2Constraint : public Constraint
{
    Q_OBJECT
    //Q_PROPERTY(RotateOrder rotateOrder READ rotateOrder WRITE setRotateOrder NOTIFY rotateOrderChanged)

    Q_PROPERTY(QVector3D linearLowerLimit READ linearLowerLimit WRITE setLinearLowerLimit NOTIFY linearLowerLimitChanged)
    Q_PROPERTY(QVector3D linearUpperLimit READ linearUpperLimit WRITE setLinearUpperLimit NOTIFY linearUpperLimitChanged)
    Q_PROPERTY(QVector3D angularLowerLimit READ angularLowerLimit WRITE setAngularLowerLimit NOTIFY angularLowerLimitChanged)
    Q_PROPERTY(QVector3D angularUpperLimit READ angularUpperLimit WRITE setAngularUpperLimit NOTIFY angularUpperLimitChanged)

    Q_PROPERTY(QVector3D pivotA READ pivotA WRITE setPivotA NOTIFY pivotAChanged)
    Q_PROPERTY(QVector3D pivotB READ pivotB WRITE setPivotB NOTIFY pivotBChanged)

    Q_PROPERTY(QQuaternion rotationA READ rotationA WRITE setRotationA NOTIFY rotationAChanged)
    Q_PROPERTY(QQuaternion rotationB READ rotationB WRITE setRotationB NOTIFY rotationBChanged)

    //in the order in world coordinate: roll(z axis)->pitch(x axis)->yaw(y axis)
    Q_PROPERTY(qreal yawA READ yawA WRITE setYawA NOTIFY yawAChanged)
    Q_PROPERTY(qreal pitchA READ pitchA WRITE setPitchA NOTIFY pitchAChanged)
    Q_PROPERTY(qreal rollA READ rollA WRITE setRollA NOTIFY rollAChanged)

    Q_PROPERTY(qreal yawB READ yawB WRITE setYawB NOTIFY yawBChanged)
    Q_PROPERTY(qreal pitchB READ pitchB WRITE setPitchB NOTIFY pitchBChanged)
    Q_PROPERTY(qreal rollB READ rollB WRITE setRollB NOTIFY rollBChanged)

    Q_PROPERTY(bool linearSpringXEnabled READ isLinearSpringXEnabled WRITE setLinearSpringXEnabled NOTIFY linearSpringXEnabledChanged)
    Q_PROPERTY(bool linearSpringYEnabled READ isLinearSpringYEnabled WRITE setLinearSpringYEnabled NOTIFY linearSpringYEnabledChanged)
    Q_PROPERTY(bool linearSpringZEnabled READ isLinearSpringZEnabled WRITE setLinearSpringZEnabled NOTIFY linearSpringZEnabledChanged)
    Q_PROPERTY(bool angularSpringXEnabled READ isAngularSpringXEnabled WRITE setAngularSpringXEnabled NOTIFY angularSpringXEnabledChanged)
    Q_PROPERTY(bool angularSpringYEnabled READ isAngularSpringYEnabled WRITE setAngularSpringYEnabled NOTIFY angularSpringYEnabledChanged)
    Q_PROPERTY(bool angularSpringZEnabled READ isAngularSpringZEnabled WRITE setAngularSpringZEnabled NOTIFY angularSpringZEnabledChanged)

    //0~? (stiff-soft(can bounce))
    Q_PROPERTY(QVector3D linearStiffness READ linearStiffness WRITE setLinearStiffness NOTIFY linearStiffnessChanged)

    Q_PROPERTY(QVector3D angularStiffness READ angularStiffness WRITE setAngularStiffness NOTIFY angularStiffnessChanged)

    Q_PROPERTY(QVector3D linearDamping READ linearDamping WRITE setLinearDamping NOTIFY linearDampingChanged)

    Q_PROPERTY(QVector3D angularDamping READ angularDamping WRITE setAngularDamping NOTIFY angularDampingChanged)

    //0~1 (no bouncing~full bouncing)
    Q_PROPERTY(QVector3D linearBounce READ linearBounce WRITE setLinearBounce NOTIFY linearBounceChanged)

    //0~1 (no bouncing~full bouncing)
    Q_PROPERTY(QVector3D angularBounce READ angularBounce WRITE setAngularBounce NOTIFY angularBounceChanged)

    Q_PROPERTY(bool linearMotorXEnabled READ isLinearMotorXEnabled WRITE setLinearMotorXEnabled NOTIFY linearMotorXEnabledChanged)
    Q_PROPERTY(bool linearMotorYEnabled READ isLinearMotorYEnabled WRITE setLinearMotorYEnabled NOTIFY linearMotorYEnabledChanged)
    Q_PROPERTY(bool linearMotorZEnabled READ isLinearMotorZEnabled WRITE setLinearMotorZEnabled NOTIFY linearMotorZEnabledChanged)
    Q_PROPERTY(bool angularMotorXEnabled READ isAngularMotorXEnabled WRITE setAngularMotorXEnabled NOTIFY angularMotorXEnabledChanged)
    Q_PROPERTY(bool angularMotorYEnabled READ isAngularMotorYEnabled WRITE setAngularMotorYEnabled NOTIFY angularMotorYEnabledChanged)
    Q_PROPERTY(bool angularMotorZEnabled READ isAngularMotorZEnabled WRITE setAngularMotorZEnabled NOTIFY angularMotorZEnabledChanged)

    Q_PROPERTY(bool linearServoXEnabled READ isLinearServoXEnabled WRITE setLinearServoXEnabled NOTIFY linearServoXEnabledChanged)
    Q_PROPERTY(bool linearServoYEnabled READ isLinearServoYEnabled WRITE setLinearServoYEnabled NOTIFY linearServoYEnabledChanged)
    Q_PROPERTY(bool linearServoZEnabled READ isLinearServoZEnabled WRITE setLinearServoZEnabled NOTIFY linearServoZEnabledChanged)
    Q_PROPERTY(bool angularServoXEnabled READ isAngularServoXEnabled WRITE setAngularServoXEnabled NOTIFY angularServoXEnabledChanged)
    Q_PROPERTY(bool angularServoYEnabled READ isAngularServoYEnabled WRITE setAngularServoYEnabled NOTIFY angularServoYEnabledChanged)
    Q_PROPERTY(bool angularServoZEnabled READ isAngularServoZEnabled WRITE setAngularServoZEnabled NOTIFY angularServoZEnabledChanged)

    Q_PROPERTY(QVector3D linearTargetVelocity READ linearTargetVelocity WRITE setLinearTargetVelocity NOTIFY linearTargetVelocityChanged)
    Q_PROPERTY(QVector3D angularTargetVelocity READ angularTargetVelocity WRITE setAngularTargetVelocity NOTIFY angularTargetVelocityChanged)

    Q_PROPERTY(QVector3D linearServoTarget READ linearServoTarget WRITE setLinearServoTarget NOTIFY linearServoTargetChanged)

    //-90 ~ 90 degrees
    Q_PROPERTY(QVector3D angularServoTarget READ angularServoTarget WRITE setAngularServoTarget NOTIFY angularServoTargetChanged)

    Q_PROPERTY(QVector3D maxLinearMotorForce READ maxLinearMotorForce WRITE setMaxLinearMotorForce NOTIFY maxLinearMotorForceChanged)
    Q_PROPERTY(QVector3D maxAngularMotorForce READ maxAngularMotorForce WRITE setMaxAngularMotorForce NOTIFY maxAngularMotorForceChanged)

public:
    explicit Generic6DofSpring2Constraint(QObject *parent = nullptr);

    Generic6DofSpring2Constraint::RotateOrder rotateOrder() const;

    QSharedPointer<btGeneric6DofSpring2Constraint> generic6DofSpring2() const;

    QVector3D linearLowerLimit() const;

    QVector3D linearUpperLimit() const;

    QVector3D angularLowerLimit() const;

    QVector3D angularUpperLimit() const;

    QVector3D pivotA() const;

    QVector3D pivotB() const;

    QQuaternion rotationA() const;

    QQuaternion rotationB() const;

    qreal yawA() const;

    qreal pitchA() const;

    qreal rollA() const;

    qreal yawB() const;

    qreal pitchB() const;

    qreal rollB() const;

    bool isLinearSpringXEnabled() const;

    bool isLinearSpringYEnabled() const;

    bool isLinearSpringZEnabled() const;

    bool isAngularSpringXEnabled() const;

    bool isAngularSpringYEnabled() const;

    bool isAngularSpringZEnabled() const;

    QVector3D linearStiffness() const;

    QVector3D angularStiffness() const;

    QVector3D linearDamping() const;

    QVector3D angularDamping() const;

    QVector3D linearBounce() const;

    QVector3D angularBounce() const;

    bool isLinearMotorXEnabled() const;

    bool isLinearMotorYEnabled() const;

    bool isLinearMotorZEnabled() const;

    bool isAngularMotorXEnabled() const;

    bool isAngularMotorYEnabled() const;

    bool isAngularMotorZEnabled() const;

    bool isLinearServoXEnabled() const;

    bool isLinearServoYEnabled() const;

    bool isLinearServoZEnabled() const;

    bool isAngularServoXEnabled() const;

    bool isAngularServoYEnabled() const;

    bool isAngularServoZEnabled() const;

    QVector3D linearTargetVelocity() const;

    QVector3D angularTargetVelocity() const;

    QVector3D linearServoTarget() const;

    QVector3D angularServoTarget() const;

    QVector3D maxLinearMotorForce() const;

    QVector3D maxAngularMotorForce() const;

signals:

    void rotateOrderChanged(Generic6DofSpring2Constraint::RotateOrder rotateOrder);

    void linearLowerLimitChanged(QVector3D linearLowerLimit);

    void linearUpperLimitChanged(QVector3D linearUpperLimit);

    void angularLowerLimitChanged(QVector3D angularLowerLimit);

    void angularUpperLimitChanged(QVector3D angularUpperLimit);

    void pivotAChanged(QVector3D pivotA);

    void pivotBChanged(QVector3D pivotB);

    void rotationAChanged(QQuaternion rotA);

    void rotationBChanged(QQuaternion rotB);

    void yawAChanged(qreal yaw);

    void pitchAChanged(qreal pitch);

    void rollAChanged(qreal roll);

    void yawBChanged(qreal yaw);

    void pitchBChanged(qreal pitch);

    void rollBChanged(qreal roll);

    void linearSpringXEnabledChanged(bool linearSpringXEnabled);

    void linearSpringYEnabledChanged(bool linearSpringYEnabled);

    void linearSpringZEnabledChanged(bool linearSpringZEnabled);

    void angularSpringXEnabledChanged(bool angularSpringXEnabled);

    void angularSpringYEnabledChanged(bool angularSpringYEnabled);

    void angularSpringZEnabledChanged(bool angularSpringZEnabled);

    void linearStiffnessChanged(QVector3D linearStiffness);

    void angularStiffnessChanged(QVector3D angularStiffness);

    void linearDampingChanged(QVector3D linearDamping);

    void angularDampingChanged(QVector3D angularDamping);

    void linearBounceChanged(QVector3D linearBounce);

    void angularBounceChanged(QVector3D angularBounce);

    void linearMotorXEnabledChanged(bool linearMotorXEnabled);

    void linearMotorYEnabledChanged(bool linearMotorYEnabled);

    void linearMotorZEnabledChanged(bool linearMotorZEnabled);

    void angularMotorXEnabledChanged(bool angularMotorXEnabled);

    void angularMotorYEnabledChanged(bool angularMotorYEnabled);

    void angularMotorZEnabledChanged(bool angularMotorZEnabled);

    void linearServoXEnabledChanged(bool linearServoXEnabled);

    void linearServoYEnabledChanged(bool linearServoYEnabled);

    void linearServoZEnabledChanged(bool linearServoZEnabled);

    void angularServoXEnabledChanged(bool angularServoXEnabled);

    void angularServoYEnabledChanged(bool angularServoYEnabled);

    void angularServoZEnabledChanged(bool angularServoZEnabled);

    void linearTargetVelocityChanged(QVector3D linearTargetVelocity);

    void angularTargetVelocityChanged(QVector3D angularTargetVelocity);

    void linearServoTargetChanged(QVector3D linearServoTarget);

    void angularServoTargetChanged(QVector3D angularServoTarget);

    void maxLinearMotorForceChanged(QVector3D maxLinearMotorForce);

    void maxAngularMotorForceChanged(QVector3D maxAngularMotorForce);

public slots:

    void setRotateOrder(Generic6DofSpring2Constraint::RotateOrder rotateOrder);

    void setLinearLowerLimit(QVector3D linearLowerLimit);

    void setLinearUpperLimit(QVector3D linearUpperLimit);

    void setAngularLowerLimit(QVector3D angularLowerLimit);

    void setAngularUpperLimit(QVector3D angularUpperLimit);

    void setPivotA(QVector3D pivotA);

    void setPivotB(QVector3D pivotB);

    void setRotationA(QQuaternion rotA);

    void setRotationB(QQuaternion rotB);

    void setYawA(qreal yaw);

    void setPitchA(qreal pitch);

    void setRollA(qreal roll);

    void setYawB(qreal yaw);

    void setPitchB(qreal pitch);

    void setRollB(qreal roll);

    QMatrix4x4 transformA() const;

    QMatrix4x4 transformB() const;

    void setLinearSpringXEnabled(bool linearSpringXEnabled);

    void setLinearSpringYEnabled(bool linearSpringYEnabled);

    void setLinearSpringZEnabled(bool linearSpringZEnabled);

    void setAngularSpringXEnabled(bool angularSpringXEnabled);

    void setAngularSpringYEnabled(bool angularSpringYEnabled);

    void setAngularSpringZEnabled(bool angularSpringZEnabled);

    void setLinearStiffness(QVector3D linearStiffness);

    void setAngularStiffness(QVector3D angularStiffness);

    void setLinearDamping(QVector3D linearDamping);

    void setAngularDamping(QVector3D angularDamping);

    void setLinearBounce(QVector3D linearBounce);

    void setAngularBounce(QVector3D angularBounce);

    void setLinearMotorXEnabled(bool linearMotorXEnabled);

    void setLinearMotorYEnabled(bool linearMotorYEnabled);

    void setLinearMotorZEnabled(bool linearMotorZEnabled);

    void setAngularMotorXEnabled(bool angularMotorXEnabled);

    void setAngularMotorYEnabled(bool angularMotorYEnabled);

    void setAngularMotorZEnabled(bool angularMotorZEnabled);

    void setLinearServoXEnabled(bool linearServoXEnabled);

    void setLinearServoYEnabled(bool linearServoYEnabled);

    void setLinearServoZEnabled(bool linearServoZEnabled);

    void setAngularServoXEnabled(bool angularServoXEnabled);

    void setAngularServoYEnabled(bool angularServoYEnabled);

    void setAngularServoZEnabled(bool angularServoZEnabled);

    void setLinearTargetVelocity(QVector3D linearTargetVelocity);

    void setAngularTargetVelocity(QVector3D angularTargetVelocity);

    //degrees
    void setLinearServoTarget(QVector3D linearServoTarget);

    //degrees
    void setAngularServoTarget(QVector3D angularServoTarget);

    void setMaxLinearMotorForce(QVector3D maxLinearMotorForce);

    void setMaxAngularMotorForce(QVector3D maxAngularMotorForce);

protected:
    QSharedPointer<btTypedConstraint> create() const override;

    void postCreate() override;

private:
    RotateOrder m_rotateOrder;
    QVector3D m_linearLowerLimit;

    QVector3D m_linearUpperLimit;
    QVector3D m_angularLowerLimit;
    QVector3D m_angularUpperLimit;

    QVector3D m_pivotA;
    QVector3D m_pivotB;
    QQuaternion m_rotA, m_rotB;

    bool m_linearSpringXEnabled;
    bool m_linearSpringYEnabled;
    bool m_linearSpringZEnabled;
    bool m_angularSpringXEnabled;
    bool m_angularSpringYEnabled;
    bool m_angularSpringZEnabled;

    QVector3D m_linearStiffness;
    QVector3D m_angularStiffness;
    QVector3D m_linearDamping;
    QVector3D m_angularDamping;
    QVector3D m_linearBounce;
    QVector3D m_angularBounce;
    bool m_linearMotorXEnabled;
    bool m_linearMotorYEnabled;
    bool m_linearMotorZEnabled;
    bool m_angularMotorXEnabled;
    bool m_angularMotorYEnabled;
    bool m_angularMotorZEnabled;

    bool m_linearServoXEnabled;
    bool m_linearServoYEnabled;
    bool m_linearServoZEnabled;
    bool m_angularServoXEnabled;
    bool m_angularServoYEnabled;
    bool m_angularServoZEnabled;

    QVector3D m_linearTargetVelocity;
    QVector3D m_angularTargetVelocity;

    QVector3D m_linearServoTarget;
    QVector3D m_angularServoTarget;

    QVector3D m_maxLinearMotorForce;
    QVector3D m_maxAngularMotorForce;
};

}


#endif // GENERIC6DOFSPRING2CONSTRAINT_H
