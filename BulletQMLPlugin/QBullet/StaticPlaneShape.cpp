/*!
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
#include <btBulletDynamicsCommon.h>

#include "QBullet.h"
#include "StaticPlaneShape.h"

namespace QBullet {

StaticPlaneShape::StaticPlaneShape(QObject *parent)
    : CollisionShape(parent)
    , m_normal(0, 1, 0)
{
}

StaticPlaneShape::~StaticPlaneShape()
{
}

QVector3D StaticPlaneShape::planeNormal() const
{
    return m_normal;
}

void StaticPlaneShape::setPlaneNormal(const QVector3D &planeNormal)
{
    if(m_normal==planeNormal)
        return;
    m_normal = planeNormal;

    resetShape();

    emit planeNormalChanged(planeNormal);
}

QSharedPointer<btCollisionShape> StaticPlaneShape::create() const
{
    return QSharedPointer<btCollisionShape>(new btStaticPlaneShape(q2b(m_normal), 0));
}

QSharedPointer<btStaticPlaneShape> StaticPlaneShape::staticPlane() const
{
    return shape().dynamicCast<btStaticPlaneShape>();
}

}

#include "moc_StaticPlaneShape.cpp"
