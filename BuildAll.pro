TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += \
    $$PWD/BulletQMLPlugin \
    $$PWD/BulletToolsQMLPlugin \
    $$PWD/RenderQMLPlugin \
    $$PWD/Example \
    $$PWD/MarbleMaze \
    $$PWD/VehicleSimulation
