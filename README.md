Bullet Physics QML Plugin
=========================

version 0.4

Bin Chen

23 Mar. 2018

Bullet Physics QML Plugin provides Qt QML wrapper classes for bullet physics API 
which can be used together with Qt3D in physics simulation software. 

The plugin is based on Qt 5.10 and Bullet 2.83.

## Supported Bullet features

------------------------------
Rigid body dynamic simulation:

* AutoHeightmapImpactModifier
* CollisionObject
* DiscreteDynamicsWorld
* RigidBody
* ContackCallback
* RayTest
* HeightmapModifier

------------------------------
[Collision shapes](http://bulletphysics.org/Bullet/BulletFull/classbtCollisionShape.html):

* BoxShape
* CapsuleShape
* CompoundShape
* ConeShape
* CylinderShape
* HeightfieldTerrainShape
* SphereShape
* StaticPlaneShape
* TriangleMeshShape
* UniformScalingShape

------------------------------
[Constraints](http://bulletphysics.org/mediawiki-1.5.8/index.php/Constraints):

see: [Bullet constrain classes](http://bulletphysics.org/Bullet/BulletFull/classbtTypedConstraint.html)

* ConeTwistConstraint
* FixedConstraint
* GearConstraint
* Generic6DofConstraint
* Generic6DofSpring2Constraint
* Generic6DofSpringConstraint
* Hinge2Constraint
* HingeAccumulatedAngleConstraint
* HingeConstraint
* Point2PointConstraint
* SliderConstraint
* UniversalConstraint

## Supported Bullet related Qt3D render features

* TriangleMesh: Defines a QML triangle mesh object which is used by TriangleMeshShape for physics simulation 
				and TriangleMeshRenderer for 3D visulization.

* TriangleMeshRenderer: render TriangleMesh.

## Dependencies and supported platforms

* Qt: 5.10 or above.
* [Bullet: 2.83](https://github.com/bulletphysics/bullet3)
* development platforms: Windows 7, osx.
* target platforms: Windows 7, osx, iOS, Android.
* gl3 and es2

## Build with Qt Creator

[root] means Bullet QML Plugin source root directory.

Before building BulletQMLPlugin you need to have bullet library ready on your development computer. 

An environment variable BULLET_PATH_ENVIRONMENT needs to set to your bullet library installation directory.
This varialbe is required by the BulletQMLPlugin to find the bullet. 

Open [root]/BuildAll.pro in Qt Creator you will see four projects:

* BulletQMLPlugin: Bullet qml wrapper build static QML plugin by default. You may change BulletQMLPlugin.pro to make dynamic plugin.

* BulletToolsQMLPlugin: Convenient qml/c++ classes/modules for testing BulletQMLPlugin.

* RenderQMLPlugin: Provides a frame graphs upport shadow rendering with shadowmap technique. It has 2 materials, DiffuseMapMaterial and MetalRoughMaterial for gl3 and es2 platforms. 

* Example: A demo for box and sphere shapes. 

* MarbleMaze: A demo for constraints and heightmap.

* VehicleSimulation: A demo for constraints, heightmap and HeightfieldTerrainShape.

## License

The Bullet Physics QML Plugin is licensed under the permissive [zlib license](https://opensource.org/licenses/zlib-license.php).

The zlib license text is included at the top of each source file:

```
BulletQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
```