//gl3/shadowmap.inc.frag

uniform sampler2DShadow shadowMapTexture;
uniform float shadowBias;

#pragma include light.inc.frag

float shadowMapFactor(
         const in vec3 worldPosition,
         const in vec3 worldNormal,
         const in vec4 positionInLightSpace
        )
{
    vec4 shadowPosition = positionInLightSpace;
    shadowPosition.z -= shadowBias;

    float shadowMapSample = textureProj(shadowMapTexture, shadowPosition);

    // Check if the fragment's x,y is outside the camera's frustum
    vec3 lightSpaceCoord = positionInLightSpace.xyz / positionInLightSpace.w;

    if(lightSpaceCoord.x < 0 || lightSpaceCoord.x >1 ||
            lightSpaceCoord.y < 0 || lightSpaceCoord.y >1 ||
            lightSpaceCoord.z <= 0.1 || lightSpaceCoord.z > 1) {
        shadowMapSample = 1.0;
    }

    /*
    Only works for first light as the shadow map light.

    If world normal is pointing away from shadow light direction, the shadow
    should not be drawn.
    */
    vec3 s;
    if(lights[0].type != TYPE_DIRECTIONAL) {
        s = normalize(lights[0].position - worldPosition);
    } else {
        s = -lights[0].direction;
    }

    float sDotN = dot(s, worldNormal);
    if (sDotN<=0) {
        shadowMapSample = 1.0;
    }
    return shadowMapSample;
}
