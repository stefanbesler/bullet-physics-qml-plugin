#version 150 core
//gl3/metalRough.frag

uniform vec4 ambientColor;            // Ambient reflectivity
uniform float metalness = 0.5;
uniform float roughness = 0.5;
uniform float ambientOcclusion = 1.0;

uniform sampler2D diffuseTexture;
uniform vec4 diffuseColor;

uniform float alphaValue;

in vec3 worldPosition;
in vec3 worldNormal;
in vec3 worldView;
in vec4 positionInLightSpace;
in vec2 texCoord;

out vec4 fragColor;

#pragma include metalrough.inc.frag
#pragma include shadowmap.inc.frag

void main()
{
    // Get the texture color and perform any colorization required.
    vec4 baseColor = texture(diffuseTexture, texCoord) * diffuseColor;

    float tmpAmbientOcclusion = 1.0;
    if (shadowMapFactor(worldPosition, worldNormal, positionInLightSpace) > 0.0) {
        //Not in shadow
    } else {
        tmpAmbientOcclusion = ambientColor.r;
    }

    baseColor = metalRoughFunction(baseColor,
                                   metalness,
                                   roughness,
                                   tmpAmbientOcclusion,
                                   worldPosition,
                                   worldView,
                                   worldNormal
                                   );

    baseColor.a *= alphaValue;
    fragColor = baseColor;
}
