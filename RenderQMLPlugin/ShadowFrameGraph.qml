import Qt3D.Render 2.10 as Render

Render.RenderSurfaceSelector {
    id: root
    //Can not use 4096 on Samsung s8, 2048 is fine.
    property int shadowMapSize: 1024

    property alias clearColor: clearBuffers.clearColor
    property alias lightCamera: lightCameraSelector.camera
    property alias depthTexture: depthTexture

    Render.Viewport {
        normalizedRect: Qt.rect(0,0,1,1)
        Render.FrustumCulling {
            enabled: true

            Render.TechniqueFilter {
                matchAll: [
                    // Materials' effects from the Extras module use this filterKey in its techniques
                    Render.FilterKey { name: "renderingStyle"; value: "shadowRender"}
                ]

                Render.RenderPassFilter {
                    matchAny: [ Render.FilterKey { name: "pass"; value: "shadowmap" } ]

                    Render.RenderTargetSelector {
                        target: Render.RenderTarget {
                            attachments: [
                                Render.RenderTargetOutput {
                                    objectName: "depth"
                                    attachmentPoint: Render.RenderTargetOutput.Depth
                                    texture: Render.Texture2D {
                                        id: depthTexture
                                        width: root.shadowMapSize
                                        height: root.shadowMapSize
                                        //format: Render.Texture.DepthFormat
                                        format: Render.Texture.D16
                                        generateMipMaps: false
                                        magnificationFilter: Render.Texture.Linear
                                        minificationFilter: Render.Texture.Linear
                                        wrapMode {
                                            x: Render.WrapMode.ClampToEdge
                                            y: Render.WrapMode.ClampToEdge
                                        }
                                        comparisonFunction: Render.Texture.CompareLessEqual
                                        comparisonMode: Render.Texture.CompareRefToTexture
                                    }
                                }
                            ]
                        }

                        Render.ClearBuffers {
                            buffers: Render.ClearBuffers.DepthBuffer

                            Render.CameraSelector {
                                id: lightCameraSelector
                            }
                        }
                    }
                }
            }

            Render.ClearBuffers {
                id: clearBuffers
                buffers: Render.ClearBuffers.ColorDepthBuffer
                clearColor: "black" // default, overriden by alias

                Render.NoDraw {} // just clear the buffers
            }

        }

    }

}
