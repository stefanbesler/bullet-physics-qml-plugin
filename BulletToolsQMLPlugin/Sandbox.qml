/*!
BulletToolsQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Render 2.0

import QBullet 1.0 as Bullet
import QRender 1.0 as QRender

Entity {
    id: root
    property Bullet.DiscreteDynamicsWorld world: null
    property vector3d origin: Qt.vector3d(0, 0, 0)
    property quaternion rotation: Qt.quaternion(1, 0, 0, 0)
    property real width: 200
    property real wallWidth: 2
    property real wallHeight: 10
    property real wallOffset: (width - wallWidth)/2
    property bool enabledSpring: true
    property alias heightmap: heightmap.heightmap
    property vector3d scale: Qt.vector3d(1, 1, 1)
    property alias diamondSubdivision: heightmap.diamondSubdivision
    property bool inContainer: true
    property alias friction: body.friction
    property alias rollingFriction: body.rollingFriction
    property alias spinningFriction: body.spinningFriction
    property alias restitution: body.restitution

    property Material material: QRender.MetalRoughMaterial {
        ambient: "#808080"
        metalness: 0.9
        roughness: 0.1
        alpha: 1
        textureScale: 5
        diffuse: "#FFFFFF"

        diffuseTexture: TextureLoader {
            source: "qrc:/resources/earth_0.png"
            generateMipMaps: true
            minificationFilter: Texture.LinearMipMapLinear
            magnificationFilter: Texture.Linear
            maximumAnisotropy: 16.0
            wrapMode {
                x: WrapMode.Repeat
                y: WrapMode.Repeat
                z: WrapMode.Repeat
            }
        }

    }

    Bullet.Heightmap {
        id: heightmap
        //heightmap: "qrc:/resources/Heightmap.png"
        scale: heightmap.valid?root.scale.times(Qt.vector3d(root.width/heightmap.width, root.wallHeight/256, root.width/heightmap.height)):
                                 Qt.vector3d(1, 1, 1)
    }

    Bullet.CompoundShape {
        id: groundShape
        shapes: [
            Bullet.BoxShape {
                id: groundBoxShape
                dimensions: inContainer?Qt.vector3d(root.width, 1, root.width):Qt.vector3d(0, 0, 0)
            },
            Bullet.TriangleMeshShape {
                id: meshShape
                meshes: [heightmap]
            },
            Bullet.BoxShape {
                id: northWallShape
                dimensions: inContainer?Qt.vector3d(root.width, root.wallHeight, root.wallWidth):Qt.vector3d(0, 0, 0)
                Bullet.Compound.position: inContainer?Qt.vector3d(0, wallHeight/2, -root.wallOffset):Qt.vector3d(0, 0, 0)
            },
            Bullet.BoxShape {
                id: southWallShape
                dimensions: inContainer?Qt.vector3d(root.width, root.wallHeight, root.wallWidth):Qt.vector3d(0, 0, 0)
                Bullet.Compound.position: inContainer?Qt.vector3d(0, wallHeight/2, root.wallOffset):Qt.vector3d(0, 0, 0)
            },
            Bullet.BoxShape {
                id: eastWallShape
                dimensions: inContainer?Qt.vector3d(root.wallWidth, root.wallHeight, root.width):Qt.vector3d(0, 0, 0)
                Bullet.Compound.position: inContainer?Qt.vector3d(root.wallOffset, wallHeight/2, 0):Qt.vector3d(0, 0, 0)
            },
            Bullet.BoxShape {
                id: westWallShape
                dimensions: inContainer?Qt.vector3d(root.wallWidth, root.wallHeight, root.width):Qt.vector3d(0, 0, 0)
                Bullet.Compound.position: inContainer?Qt.vector3d(-root.wallOffset, wallHeight/2, 0):Qt.vector3d(0, 0, 0)
            }
        ]
    }

    Bullet.RigidBody {
        id: body
        world: root.world
        collisionShape: groundShape
        origin: root.origin.plus(Qt.vector3d(0, -groundBoxShape.dimensions.y, 0))
        rotation: root.rotation
        mass: enabledSpring?100:0
        restitution: 0

    }

    Bullet.BoxShape {
        id: baseShape
        dimensions: Qt.vector3d(root.width, 10, root.width)
    }

    Bullet.RigidBody {
        id: base
        collisionShape: baseShape
        mass: enabledSpring?100:0
        world: root.world
        origin: Qt.vector3d(0, -root.width, 0)
    }

    Bullet.HingeConstraint {
        world: root.world
        rigidBodyA: base
        axisA: Qt.vector3d(0, 1, 0)
        pivotA: Qt.vector3d(0, 0, 0)
    }

    Bullet.Generic6DofSpring2Constraint {
        world: root.world
        rigidBodyA: base
        rigidBodyB: body
        pivotA: Qt.vector3d(0, root.width, 0)
        pivotB: Qt.vector3d(0, -groundBoxShape.dimensions.y/2, 0)
        angularLowerLimit: Qt.vector3d(-45, 0, -45)
        angularUpperLimit: Qt.vector3d(45, 0, 45)
        angularMotorXEnabled: true
        angularMotorZEnabled: true
        angularServoXEnabled: true
        angularServoZEnabled: true
        angularTargetVelocity: Qt.vector3d(50, 0, 50)
        angularServoTarget: Qt.vector3d(0, 0, 0)
        maxAngularMotorForce: Qt.vector3d(100000, 0, 100000)
    }

    Entity {
        components: [
            Transform {
                matrix: body.matrix
            }
        ]
        Entity {
            //Heightmap Mesh
            //enabled: false
            Bullet.TriangleMeshRenderer {
                id: heightmapRenderer
                mesh: heightmap
            }
            components: [heightmapRenderer, root.material]
        }

        QRender.Box {
            matrix: groundBoxShape.Bullet.Compound.matrix
            dimensions: groundBoxShape.dimensions
            material: root.material
        }

        QRender.Box {
            matrix: northWallShape.Bullet.Compound.matrix
            dimensions: northWallShape.dimensions
            material: root.material
        }
        QRender.Box {
            matrix: southWallShape.Bullet.Compound.matrix
            dimensions: southWallShape.dimensions
            material: root.material
        }
        QRender.Box {
            matrix: eastWallShape.Bullet.Compound.matrix
            dimensions: eastWallShape.dimensions
            material: root.material
        }
        QRender.Box {
            matrix: westWallShape.Bullet.Compound.matrix
            dimensions: westWallShape.dimensions
            material: root.material
        }
    }

}
