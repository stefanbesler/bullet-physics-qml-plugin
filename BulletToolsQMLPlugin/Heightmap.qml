/*!
BulletToolsQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Render 2.0

import QtQml.Models 2.2
import QBullet 1.0 as QB
import QRender 1.0 as QRender
import "utils.js" as Utils

Entity {
    function testDynamicChange() {
        heightmap.testDynamicChange();
    }

    function substract(center, radius) {
        heightmap.substract(meshBody.world2Local(center).plus(Qt.vector3d(0, heightmap.halfHeightValue, 0)), radius);
    }

    id: root
    property QB.DiscreteDynamicsWorld world: null
    property alias position: meshBody.origin
    property alias heightmap: heightmap.heightmap
    property alias scale: heightmap.scale
    property alias body: meshBody
    property alias friction: meshBody.friction
    property alias restitution: meshBody.restitution

    property Material material: QRender.DiffusemapMaterial {
        id: material
        ambient: "#808080"
        alpha: 1

        cullingMode: CullFace.NoCulling

        textureScale: 10

        // only used if there is no diffuseTexture supplied
        diffuse: "#FFFFFF"

        diffuseTexture: TextureLoader {
            source: "qrc:/resources/earth_0.png"
            generateMipMaps: true
            minificationFilter: Texture.LinearMipMapLinear
            magnificationFilter: Texture.Linear
            maximumAnisotropy: 16.0
            wrapMode {
                x: WrapMode.Repeat
                y: WrapMode.Repeat
                z: WrapMode.Repeat
            }
        }
    }

    QB.Heightmap {
        id: heightmap
        diamondSubdivision: true
        heightmap: "qrc:/resources/Heightmap.png"
    }

    //    QB.TriangleMeshShape {
    //        id: heightmapShape
    //        meshes: [heightmap]
    //    }

    QB.HeightfieldTerrainShape {
        id: heightmapShape
        heightmap: heightmap
        scale: heightmap.scale
    }

    QB.RigidBody {
        id: meshBody
        collisionShape: heightmapShape
        world: root.world
    }

    Entity {

        components: [
            Transform {
                matrix: meshBody.matrix
            }]

        Entity {
            QB.TriangleMeshRenderer {
                id: meshRenderer
                mesh: heightmap
            }

            Transform {
                id: transform1
                translation: Qt.vector3d(0, -heightmap.halfHeightValue, 0)
            }

            components: [transform1, meshRenderer, root.material]

        }
    }
}
