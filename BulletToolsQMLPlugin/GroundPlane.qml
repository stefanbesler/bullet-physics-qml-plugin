/*!
BulletToolsQMLPlugin

Copyright (c) 2018

Bin Chen

This software is provided 'as-is', without any express or implied warranty. In
no event will the authors be held liable for any damages arising from the use
of this software. Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
that you wrote the original software. If you use this software in a product, an
acknowledgment in the product documentation would be appreciated but is not
required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.
*/
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0
import QBullet 1.0 as QB
import QRender 1.0 as QRender

Entity {
    id: root
    property QB.DiscreteDynamicsWorld world: null
    property vector3d position: Qt.vector3d(0, 0, 0)
    property quaternion rotation: Qt.quaternion(1, 0, 0, 0)
    ////////////////////////////////////////////////////////////////////////////////
    //Use static plane shape.
    //    property Material material: QRender.MetalRoughMaterial {
    //        // Note: that DiffuseMapMaterial doesn't have RenderPasses so won't support shadow
    //        // generation/detection out of the box. To do this, we will have to build an equivalent
    //        // material with additional render passes.

    //        ambient: "#808080"
    //        specular: "black"
    //        shininess: 0

    //        alpha: 1
    //        
    //        

    //        textureScale: 25.6

    //        // only used if there is no diffuseTexture supplied
    //        diffuse: "#FFFFFF"

    //        diffuseTexture: TextureLoader {
    //            source: "qrc:/resources/earth_0.png"
    //            generateMipMaps: true
    //            minificationFilter: Texture.LinearMipMapLinear
    //            magnificationFilter: Texture.Linear
    //            maximumAnisotropy: 16.0
    //            wrapMode {
    //                x: WrapMode.Repeat
    //                y: WrapMode.Repeat
    //                z: WrapMode.Repeat
    //            }
    //        }

    //    }

    //    QB.StaticPlaneShape {
    //        id: planeShape
    //    }

    //    QB.RigidBody {
    //        id: body
    //        origin: root.position
    //        rotation: root.rotation
    //        collisionShape: planeShape
    //        restitution: 0
    //        world: root.world
    //        mass: 0
    //    }

    //    Transform {
    //        id: transform
    //        translation: body.origin
    //        rotation: body.rotation
    //    }

    //    PlaneMesh {
    //        id: mesh
    //        width: 300
    //        height: width
    //        meshResolution: Qt.size(2, 2)
    //    }

    //    components: [
    //        mesh,
    //        transform,
    //        material
    //    ]
    ////////////////////////////////////////////////////////////////////////////////
    //Use box shape.
    QB.BoxShape {
        id: boxShape
        dimensions: Qt.vector3d(1000, 1, 1000);
    }

    QB.RigidBody {
        id: body
        world: root.world
        collisionShape: boxShape
        origin: root.position.plus(Qt.vector3d(0, -boxShape.dimensions.y, 0))
        rotation: root.rotation
        mass: 0
        restitution: 0.1
        friction: 1
    }

    QRender.Box {
        matrix: body.matrix
        dimensions: boxShape.dimensions
        //QQ2.Component.onCompleted: console.log(position)
        material: QRender.MetalRoughMaterial {
            // Note: that DiffuseMapMaterial doesn't have RenderPasses so won't support shadow
            // generation/detection out of the box. To do this, we will have to build an equivalent
            // material with additional render passes.

            ambient: "#808080"
            alpha: 1
            roughness: 1
            metalness: 0
            

            // only used if there is no diffuseTexture supplied
            textureScale: 50

            // only used if there is no diffuseTexture supplied
            diffuse: "#FFFFFF"

            diffuseTexture: TextureLoader {
                source: "qrc:/resources/earth_0.png"
                generateMipMaps: true
                minificationFilter: Texture.LinearMipMapLinear
                magnificationFilter: Texture.Linear
                maximumAnisotropy: 16.0
                wrapMode {
                    x: WrapMode.Repeat
                    y: WrapMode.Repeat
                    z: WrapMode.Repeat
                }
            }

        }
    }


}
